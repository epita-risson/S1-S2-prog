#load "graphics.cma" ;;
#load "unix.cma" ;;
#load "threads.cma" ;;
open Graphics ;;
open_graph "" ;;
resize_window 1000 1000 ;;
set_color black ;;
clear_graph ();;
fill_rect 0 0 1000 1000;;

let rec koch (a_x,a_y) (b_x, b_y) n =
  set_color (rgb (Random.int 256) (Random.int 256) (Random.int 256));
  moveto (int_of_float a_x) (int_of_float a_y);
  if n = 0 then lineto (int_of_float b_x) (int_of_float b_y)
  else
    let c_x = a_x +. (b_x -. a_x) /. 3. in
    let c_y = a_y +. (b_y -. a_y) /. 3. in
    let d_x = 2. *. c_x -. a_x in
    let d_y = 2. *. c_y -. a_y in
    let e_x = c_x +. (d_x -. c_x) /. 2. +. (d_y -. c_y) *. (sqrt 3.) /. 2. in
    let e_y = c_y -. (d_x -. c_x) *. (sqrt 3.) /. 2. +. (d_y -. c_y) /. 2. in
    koch (a_x, a_y) (c_x, c_y) (n-1);
    koch (c_x, c_y) (e_x, e_y) (n-1);
    koch (e_x, e_y) (d_x, d_y) (n-1);
    koch (d_x, d_y) (b_x, b_y) (n-1)

;;

let rec draw nn =
  begin
    koch (0., 0.) (1000., 1000.) nn;
    koch (1000., 1000.) (0., 0.) nn;
    koch (1000., 0.) (0., 1000.) nn;
    koch (0., 1000.) (1000., 0.) nn;
    koch (500., 0.) (500., 1000.) nn;
    koch (500., 1000.) (500., 0.) nn;
    koch (0., 500.) (1000., 500.) nn;
    koch (1000., 500.) (0., 500.) nn;
    koch (250., 0.) (750., 1000.) nn;
    koch (750., 1000.) (250., 0.) nn;
    koch (750., 0.) (250., 1000.) nn;
    koch (250., 1000.) (750., 0.) nn;
    koch (0., 250.) (1000., 750.) nn;
    koch (1000., 750.) (0., 250.) nn;
    koch (0., 750.) (1000., 250.) nn;
    koch (1000., 250.) (0., 750.) nn;

    koch (0., 0.) (1000., 1000.) (nn-1);
    koch (1000., 1000.) (0., 0.) (nn-1);
    koch (1000., 0.) (0., 1000.) (nn-1);
    koch (0., 1000.) (1000., 0.) (nn-1);
    koch (500., 0.) (500., 1000.) (nn-1);
    koch (500., 1000.) (500., 0.) (nn-1);
    koch (0., 500.) (1000., 500.) (nn-1);
    koch (1000., 500.) (0., 500.) (nn-1);
    koch (250., 0.) (750., 1000.) (nn-1);
    koch (750., 1000.) (250., 0.) (nn-1);
    koch (750., 0.) (250., 1000.) (nn-1);
    koch (250., 1000.) (750., 0.) (nn-1);
    koch (0., 250.) (1000., 750.) (nn-1);
    koch (1000., 750.) (0., 250.) (nn-1);
    koch (0., 750.) (1000., 250.) (nn-1);
    koch (1000., 250.) (0., 750.) (nn-1);

    koch (0., 0.) (1000., 1000.) (nn-2);
    koch (1000., 1000.) (0., 0.) (nn-2);
    koch (1000., 0.) (0., 1000.) (nn-2);
    koch (0., 1000.) (1000., 0.) (nn-2);
    koch (500., 0.) (500., 1000.) (nn-2);
    koch (500., 1000.) (500., 0.) (nn-2);
    koch (0., 500.) (1000., 500.) (nn-2);
    koch (1000., 500.) (0., 500.) (nn-2);
    koch (250., 0.) (750., 1000.) (nn-2);
    koch (750., 1000.) (250., 0.) (nn-2);
    koch (750., 0.) (250., 1000.) (nn-2);
    koch (250., 1000.) (750., 0.) (nn-2);
    koch (0., 250.) (1000., 750.) (nn-2);
    koch (1000., 750.) (0., 250.) (nn-2);
    koch (0., 750.) (1000., 250.) (nn-2);
    koch (1000., 250.) (0., 750.) (nn-2);
    draw nn;
  end

  
;;






draw 5;;
