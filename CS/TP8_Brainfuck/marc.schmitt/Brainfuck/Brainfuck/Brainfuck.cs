﻿using System;
using System.Collections.Generic;

namespace Brainfuck
{
    class Brainfuck
    {
        private static string InterpretRec(string code, Dictionary<char, char> symbols, ref int pointer, ref int[] cells, int length)
        {
            var result = "";
            for(var i = 0; i < code.Length; i++)
            {
                var sym = code[i];
                
                if (sym == symbols['>'])
                {
                    pointer = (pointer + 1) % length;
                }
                else if (sym == symbols['<'])
                {
                    pointer = (pointer - 1) % length;
                }
                else if (sym == symbols['+'])
                {
                    cells[pointer] = (cells[pointer] + 1) % 256;
                }
                else if (sym == symbols['-'])
                {
                    cells[pointer] = (cells[pointer] - 1) % 256;
                }
                else if (sym == symbols['.'])
                {
                    result += (char) cells[pointer];
                }
                else if (sym == symbols[','])
                {
                    cells[pointer] = Console.Read();
                }
                else if (sym == symbols['['])
                {
                    var loop = "";
                    var x = i + 1;
                    var matching = 0;
                    for (; code[x] != symbols[']'] || matching != 0; x++)
                    {
                        if (code[x] == symbols['['])
                        {
                            matching++;
                        }
                        if (code[x] == symbols[']'] && matching != 0)
                        {
                            matching--;
                        }
                        loop += code[x];
                    }
                    i = x;
                    while (cells[pointer] != 0)
                    {
                        result += InterpretRec(loop, symbols, ref pointer, ref cells, length);
                    }
                }
                else if (sym == symbols[']'])
                {
                    return result;
                }
                else
                {
                    throw new ArgumentException("Bad character: " + sym);
                }
            }
            return result;
        }
        
        /**
         * Interpret @code with @symbols and return the resulting string.
         * In case of error, null is return.
         * Possible error cases are:
         *     - Invalid braces number / order
         *     - Symbol not present
         */
        public static string Interpret(string code, Dictionary<char, char> symbols)
        {
            // Create cells array  and initialize it to 0
            // Start position pointer at the center
            // Do not forget to use a stack for the position
            // Interpret the symbols
            // Fill the resulting string
            const int length = 30000;
            var cells = new int[length];
            for (var i = 0; i < length; i++)
            {
                cells[i] = 0;
            }
            var pointer = length / 2;
            try
            {
                return InterpretRec(code, symbols, ref pointer, ref cells, length);
            }
            catch (KeyNotFoundException)
            {
                throw new ArgumentException("Symbol not found in dictionary.", nameof(symbols));
            }
        }
        
        // Generate a string containing n times the char provided
        private static string GenString(char s, int n)
        {
            if (n <= 0)
            {
                return "";
            }
            var r = "";
            for (var i = 0; i < n; i++)
            {
                r = r + s;
            }
            return r;
        }
        
        /**
         * Generate the brainfuck code to print @text with @symbols.and return it.
         * In case of error, null is return
         * Possible error case is:
         *     - Missing symbol in @symbols
         */
        public static string GenerateCodeFromText(string text, Dictionary<char, char> symbols)
        {
            // A naive implementation could be to generate a loop foreach character in the
            // text and the print it.
            var result = "";
            try
            {
                foreach (var c in text)
                {
                    result += GenString(symbols['+'], c) + symbols['.'] + symbols['['] + symbols['-'] +
                              symbols[']'];
                }
            }
            catch (KeyNotFoundException)
            {
                throw new ArgumentException("Symbol not found in dictionary.", nameof(symbols));
            }
            return ShortenCode(result, symbols);
        }
        
         /**
         * Shorten @program with @symbols and return the resulting string.
         * In case of error, null is return.
         * Possible error cases are:
         *     - Symbol not present
         *     - Invalid symbol
         */
        public static string ShortenCode(string program, Dictionary<char, char> symbols)
        {
            // Search for the symbols['+'] sequencies and reduces it with a loop.
            // Do not hesitate to search and find more techniques.
            return program;
        }
    }
}
