﻿namespace Debugger
{
    public class Debugging
    {
        // Returns the first divisor of 666 superior or equal to 42
        public static int ex1()
        {
            var div = 42;
            while (!Misc.isDivisor(666, div))
            {
                div++;
            }
            return div;

        }

        // Returns the sum of [x - x^3 + x^5 + ...] with n number of terms
        public static double ex2(int x, int n)
        {
            double sum = 0;
            for (var i = 1; i <= n; i += 2)
            {
                sum += Loop.Power(-1, (i - 1) / 2) * Loop.Power(x, i);
            }
            return sum;
        }

        // Checks whether or not a number can be expressed as the sum of two primes
        public static bool ex3(int n)
        {
            for (var i = 2; i <= n - 2; i++)
            {
                if (Misc.IsPrime(i) && Misc.IsPrime(n - i))
                {
                    return true;
                }
            }
            return false;
        }
        
        // Sorts an int array
        public static int[] ex4(int[] arr)
        {
            for (var i = 0; i < arr.Length; i++)
            {
                for (var j = 0; j < arr.Length - 1; j++)
                {
                    if (arr[j] > arr[j + 1])
                    {
                        var temp = arr[j + 1];
                        arr[j + 1] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
            return arr;
        }
    }
}