﻿using System;

namespace Debugger
{
    public static class Misc
    {
        // Checks if b is a divisor of a
        public static bool isDivisor(int a, int b)
        {
            return a % b == 0;
        }

        // Prints an Array of int
        public static void printArr(int[] arr)
        {
            foreach (var element in arr)
            {
                Console.Write(element + " ");
            }
            Console.WriteLine();
        }
        
        // Checks whether a number is prime or not, using the AKS primality test
        public static bool IsPrime(int n)
        {
            if (n <= 1)
            {
                return false;
            }
            else if (n <= 3)
            {
                return true;
            }
            else if (n % 2 == 0 || n % 3 == 0)
            {
                return false;
            }
            var i = 5;
            while (i * i <= n)
            {
                if (n % i == 0 || n % (i + 2) == 0)
                {
                    return false;
                }
                i += 6;
            }
            return true;
        }
        
        // Generates a string containing n times the string provided
        public static string GenerateString(string s, int n, string sep="")
        {
            var r = "";
            for (var i = 0; i < n; i++)
            {
                r = r + sep + s;
            }
            return r;
        }
        
        // Checks if a number is strong, as per the definition provided by the subject
        public static bool IsStrong(int n)
        {
            long tot = 0;
            var s = n.ToString();
            foreach (var number in s)
            {
                tot += Loop.Factorial(int.Parse(number.ToString()));
            }
            return tot == n ? true : false;
        }
    }
}