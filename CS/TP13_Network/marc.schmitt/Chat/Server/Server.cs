﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Server
{
    public class Server
    {
        private readonly int _port;
        private readonly Socket _sock;
        private readonly List<Socket> _clients;
        private readonly int _waitingListSize = 10;

        public void Run()
        {
            try
            {
                Init();
                while (true)
                {
                    var client = Accept();
                    new Thread(() => HandleClient(client)) {IsBackground = true}.Start();
                    Console.WriteLine("[" + client.RemoteEndPoint + "] " + "Connected");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                _sock.Close();
            }
        }

        public void HandleClient(Socket client)
        {
            try
            {
                while (true)
                {
                    ReceiveMessages(client);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("[" + client.RemoteEndPoint + "] " + e.Message);
                client.Close();
            }
        }

        public Server(int port)
        {
            _sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _clients = new List<Socket>();
            _port = port;
        }

        public void Init()
        {
            _sock.Bind(new IPEndPoint(IPAddress.Any, _port));
            _sock.Listen(_waitingListSize);
            Console.WriteLine("Server started");
        }

        public Socket Accept()
        {
            var accepted = _sock.Accept();
            _clients.Add(accepted);
            return accepted;
        }

        public void ReceiveMessages(Socket client)
        {
            var bytes = new byte[1024];
            var size = client.Receive(bytes, SocketFlags.None);
            var str = Encoding.ASCII.GetString(bytes).Substring(0,1024);
            SendMessage(str, client);
        }

        public void SendMessage(string message, Socket sender)
        {			
            var bytes = Encoding.ASCII.GetBytes(message.Substring(0,1024));

            foreach (var client in _clients)
            {
                if (client != sender && client.Connected)
                {
                    client.Send(bytes, SocketFlags.None);
                }
            }
        }
    }
}

