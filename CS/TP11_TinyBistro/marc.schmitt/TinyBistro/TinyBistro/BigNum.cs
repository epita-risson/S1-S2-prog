﻿using System;
using System.Collections.Generic;

namespace TinyBistro
{
    /*! The BigNum class */
    /*!
     * Use to manipulate very large integers (positive and negative)
     *
     * The source code is categorized using #regions in order to improve readability
     *
     * The Program.cs was NOT updated to take into consideration the bonuses implemented, so a Tests.cs file is provided, however, in order for the project to build, it was excluded from the project.
     * To use said file, add it to the Project and install NUnit 3.10.1 using NuGet, then you can run all of the Unit Tests, and even add some !
     *
     * \author marc.schmitt
     */
    public class BigNum
    {
        #region REMARKABLE

        /** Standard BigNum value for 0 */
        public static readonly BigNum BigNumZero = new BigNum(0);

        /** Standard BigNum value for 1 */
        public static readonly BigNum BigNumOne = new BigNum(1);

        #endregion

        #region ATTRIBUTES

        /** List containing the digits of the number represented */
        private readonly List<int> _digits;

        /** Represent the sign of the number. True if the number is negative */
        private bool _sign;

        #endregion

        #region CONSTRUCTOR

        //! Constructor using a string
        /*!
         * \param number a number represented by a string (e.g.: "123456", "-42")
         */
        public BigNum(string number)
        {
            _digits = new List<int>();
            if (number == "0" || number == "-0") return;
            if (number == "") throw new ArgumentException("number cannot be empty", nameof(number));
            for (var i = number.Length - 1; i >= 0; i--)
            {
                if (i == 0 && number[i] == '-')
                {
                    _sign = true;
                    continue;
                }

                if (number[i] < 48 || number[i] > 57)
                    throw new ArgumentException("number contains an invalid character", nameof(number));
                AddDigit(int.Parse(number[i].ToString()));
            }
        }

        //! Constructor using a list of digits and the sign of the number
        /*!
         * \param number list of uint (in reversed order, i.e. 123 will be represented as [3, 2, 1])
         * \param sign sign of the number(true for negative, default is false)
         */
        public BigNum(List<uint> number, bool sign = false)
        {
            _digits = new List<int>();
            foreach (var digit in number) _digits.Add((int) digit);

            _sign = sign;
        }

        //! Constructor using a BigNum object
        /*!
         * \param number a BigNum object, which will be copied
         */
        public BigNum(BigNum number) : this(number.ToString()) {}

        //! Constructor using an int
        /*!
         * \param number an integer number ,positive or negative
         */
        public BigNum(int n) : this(n.ToString()) {}

        #endregion

        #region PROPERTIES

        //! Return the number of digits in the number
        /*!
         * \return the number of digits in the number
         */
        public int GetNumDigits()
        {
            var n = _digits.Count;
            return n == 0 ? 1 : n;
        }

        //! Check if the number is negative
        /*!
         * \return true if the number is negative, false if positive or equal to zero
         */
        public bool IsNeg()
        {
            return _sign;
        }

        //! Return the BigNum of digits in the number
        /*!
         * \return the BigNum of digits in the number
         */
        public BigNum GetBigNumDigits()
        {
            return new BigNum(_digits.Count.ToString());
        }

        //! Create a string representation of a BigNum, non-static version
        /*!
         * \return a string containing the number represented by the BigNum object
         */
        public new string ToString()
        {
            if (GetNumDigits() == 1 && GetDigit(0) == 0) return "0";

            var result = "";
            if (_sign) result += "-";
            for (var i = _digits.Count - 1; i >= 0; i--) result += _digits[i];
            return result;
        }

        //! Create a string representation of a BigNum, static version
        /*!
         * \param number the BigNum we want the string representation of
         * \return a string containing the number represented by the BigNum object
         */
        public static string ToString(BigNum number)
        {
            return number.ToString();
        }

        //! Print a BigNum
        public void Print()
        {
            Console.WriteLine(ToString());
        }
        
        //! Return an integer that indicates the sign of a BigNum, non-static version
        /*!
         * \param number the number we want to get the sign of
         * \return a number that indicates the sign of the BigNum, as follows: -1 is less than zero, 0 is equal to zero, 1 is greater than zero
         */
        public static int Sign(BigNum number)
        {
            return number > BigNumZero ? 1 : (number < BigNumZero ? -1 : 0);
        }

        //! Return an integer that indicates the sign of a BigNum, non-static version
        /*!
         * \return a number that indicates the sign of the BigNum, as follows: -1 is less than zero, 0 is equal to zero, 1 is greater than zero
         */
        public int Sign()
        {
            return Sign(this);
        }

        #endregion

        #region MODIFIERS

        //! Add a digit in front of the number (i.e. AddDigit(0) doesn't do anything)
        /*!
         * \param digit the digit to add in front of the number
         */
        public void AddDigit(int digit)
        {
            if (digit < 0 || digit > 9)
                throw new ArgumentException("digit has to be an integer between 0 and 9, included", nameof(digit));
            _digits.Add(digit);
        }

        //! Get the digit at a certain position
        /*!
         * \param position the position of the digit to get
         * \return the digit at the position
         */
        public int GetDigit(int position)
        {
            var n = GetNumDigits();
            if (n == 1 && position == 0 && _digits.Count == 0) return 0;
            if (position >= n || position < 0) throw new OverflowException("position is not in the number");
            return _digits[position];
        }

        //! Set a digit of the number
        /*!
         * Changes a digit of a number
         * \param digit the new digit
         * \param position the position the new digit will be at. Can be superior to the size of the BigNum, it will be filled with zeroes
         */
        public void SetDigit(int digit, int position)
        {
            if (digit < 0 || digit > 9)
                throw new ArgumentException("digit has to be an integer between 0 and 9, included", nameof(digit));
            if (position < 0) throw new ArgumentException("position has to be positive", nameof(position));

            if (position >= _digits.Count)
            {
                while (_digits.Count != position) _digits.Add(0);
                _digits.Add(digit);
            }
            else
            {
                _digits[position] = digit;
            }

            RemoveLeadingZeroes();
        }

        //! Remove useless zeroes in front of a BigNum
        public void RemoveLeadingZeroes()
        {
            if (_digits.Count == 0) return;
            while (_digits[_digits.Count - 1] == 0)
            {
                _digits.RemoveAt(_digits.Count - 1);
                if (_digits.Count == 0) return;
            }
        }

        //! Set the sign of the BigNum
        private void SetSign(bool sign)
        {
            _sign = sign;
        }

        #endregion

        #region OPERATORS

        #region COMPARISONS

        #region INFERIOR

        //! Comparator < for BigNums
        /*!
         * \param a first BigNum
         * \param b second BigNum
         * \return a < b
         */
        public static bool operator <(BigNum a, BigNum b)
        {
            if (a.IsNeg() && !b.IsNeg()) return true;
            if (!a.IsNeg() && b.IsNeg()) return false;
            if (a.IsNeg() && b.IsNeg()) return a.Abs() > b.Abs();

            a.RemoveLeadingZeroes();
            b.RemoveLeadingZeroes();

            var aCount = a.GetNumDigits();
            var bCount = b.GetNumDigits();

            if (aCount == 0 && bCount == 0) return false;
            if (aCount < bCount) return true;
            if (aCount > bCount) return false;

            for (var i = aCount - 1; i >= 0; i--)
            {
                if (a.GetDigit(i) < b.GetDigit(i)) return true;
                if (a.GetDigit(i) > b.GetDigit(i)) return false;
            }

            return false;
        }

        //! Comparator < for BigNum and int
        /*!
         * \param a BigNum
         * \param b int
         * \return a < b
         */
        public static bool operator <(BigNum a, int b)
        {
            return a < new BigNum(b);
        }

        //! Comparator < for BigNum and int
        /*!
         * \param a int
         * \param b BigNum
         * \return a < b
         */
        public static bool operator <(int a, BigNum b)
        {
            return new BigNum(a) < b;
        }

        #endregion

        #region SUPERIOR

        //! Comparator > for BigNums
        /*!
         * \param a first BigNum
         * \param b second BigNum
         * \return a > b
         */
        public static bool operator >(BigNum a, BigNum b)
        {
            if (a.IsNeg() && !b.IsNeg()) return false;
            if (!a.IsNeg() && b.IsNeg()) return true;
            if (a.IsNeg() && b.IsNeg()) return a.Abs() < b.Abs();

            a.RemoveLeadingZeroes();
            b.RemoveLeadingZeroes();

            var aCount = a.GetNumDigits();
            var bCount = b.GetNumDigits();

            if (aCount == 0 && bCount == 0) return false;
            if (aCount > bCount) return true;
            if (aCount < bCount) return false;

            for (var i = aCount - 1; i >= 0; i--)
            {
                if (a.GetDigit(i) > b.GetDigit(i)) return true;
                if (a.GetDigit(i) < b.GetDigit(i)) return false;
            }

            return false;
        }

        //! Comparator > for BigNum and int
        /*!
         * \param a BigNum
         * \param b int
         * \return a > b
         */
        public static bool operator >(BigNum a, int b)
        {
            return a > new BigNum(b);
        }

        //! Comparator > for BigNum and int
        /*!
         * \param a int
         * \param b BigNum
         * \return a > b
         */
        public static bool operator >(int a, BigNum b)
        {
            return new BigNum(a) > b;
        }

        #endregion

        #region EQUALS

        //! Comparator == for BigNums
        /*!
         * \param a first BigNum
         * \param b second BigNum
         * \return a == b
         */
        public static bool operator ==(BigNum a, BigNum b)
        {
            if (a.IsNeg() && !b.IsNeg() || !a.IsNeg() && b.IsNeg()) return false;

            a.RemoveLeadingZeroes();
            b.RemoveLeadingZeroes();

            var aCount = a.GetNumDigits();
            var bCount = b.GetNumDigits();

            if (aCount == 1 && bCount == 1 && a.GetDigit(0) == b.GetDigit(0)) return true;

            if (aCount != bCount) return false;

            for (var i = aCount - 1; i >= 0; i--)
                if (a.GetDigit(i) != b.GetDigit(i))
                    return false;
            return true;
        }

        //! Comparator == for BigNum and int
        /*!
         * \param a BigNum
         * \param b int
         * \return a == b
         */
        public static bool operator ==(BigNum a, int b)
        {
            return a == new BigNum(b);
        }

        //! Comparator == for BigNum and int
        /*!
         * \param a int
         * \param b BigNum
         * \return a == b
         */
        public static bool operator ==(int a, BigNum b)
        {
            return new BigNum(a) == b;
        }

        //! Check if two BigNums are equal
        /*!
         * \param number the BigNum to compare with the current one
         * \return this == number
         */
        public virtual bool Equals(BigNum number)
        {
            return this == number;
        }

        //! Check if a BigNum is equal to an int
        /*!
         * \param number the integer to compare with the BigNum
         * \return this == number
         */
        public virtual bool Equals(int number)
        {
            return this == number;
        }

        #endregion

        #region DIFFERENT

        //! Comparator != for BigNums
        /*!
         * \param a first BigNum
         * \param b second BigNum
         * \return a != b
         */
        public static bool operator !=(BigNum a, BigNum b)
        {
            return !(a == b);
        }

        //! Comparator != for BigNum and int
        /*!
         * \param a BigNum
         * \param b int
         * \return a != b
         */
        public static bool operator !=(BigNum a, int b)
        {
            return a != new BigNum(b);
        }

        //! Comparator != for BigNum and int
        /*!
         * \param a int
         * \param b BigNum
         * \return a != b
         */
        public static bool operator !=(int a, BigNum b)
        {
            return new BigNum(a) != b;
        }

        #endregion

        #region INFERIOR_EQUAL

        //! Comparator <= for BigNums
        /*!
         * \param a first BigNum
         * \param b second BigNum
         * \return a <= b
         */
        public static bool operator <=(BigNum a, BigNum b)
        {
            return a < b || a == b;
        }

        //! Comparator <= for BigNum and int
        /*!
         * \param a BigNum
         * \param b int
         * \return a <= b
         */
        public static bool operator <=(BigNum a, int b)
        {
            return a <= new BigNum(b);
        }

        //! Comparator <= for BigNum and int
        /*!
         * \param a int
         * \param b BigNum
         * \return a <= b
         */
        public static bool operator <=(int a, BigNum b)
        {
            return new BigNum(a) <= b;
        }

        #endregion

        #region SUPERIOR_EQUAL

        //! Comparator >= for BigNums
        /*!
         * \param a first BigNum
         * \param b second BigNum
         * \return a >= b
         */
        public static bool operator >=(BigNum a, BigNum b)
        {
            return a > b || a == b;
        }

        //! Comparator >= for BigNum and int
        /*!
         * \param a BigNum
         * \param b int
         * \return a >= b
         */
        public static bool operator >=(BigNum a, int b)
        {
            return a >= new BigNum(b);
        }

        //! Comparator >= for BigNum and int
        /*!
         * \param a int
         * \param b BigNum
         * \return a >= b
         */
        public static bool operator >=(int a, BigNum b)
        {
            return new BigNum(a) >= b;
        }

        #endregion
        
        #region MAX

        //! Max comparator for BigNum
        /*!
         * \param a the first BigNum to compare
         * \param b the second BigNum to compare
         * \return the biggest BigNum between a and b
         */
        public static BigNum Max(BigNum a, BigNum b)
        {
            return a < b ? new BigNum(b) : new BigNum(a);
        }
        
        //! Max comparator for BigNum
        /*!
         * \param a an int to compare
         * \param b a BigNum to compare
         * \return the biggest number between a and b
         */
        public static BigNum Max(int a, BigNum b)
        {
            return a < b ? new BigNum(b) : new BigNum(a);
        }

        //! Max comparator for BigNum
        /*!
         * \param a a BigNum to compare
         * \param b an int to compare
         * \return the biggest number between a and b
         */
        public static BigNum Max(BigNum a, int b)
        {
            return a < b ? new BigNum(b) : new BigNum(a);
        }

        #endregion
        
        #region MIN
        
        //! Min comparator for BigNum
        /*!
         * \param a the first BigNum to compare
         * \param b the second BigNum to compare
         * \return the smallest BigNum between a and b
         */
        public static BigNum Min(BigNum a, BigNum b)
        {
            return a < b ? new BigNum(a) : new BigNum(b);
        }
        
        //! Min comparator for BigNum
        /*!
         * \param a an int to compare
         * \param b a BigNum to compare
         * \return the smallest number between a and b
         */
        public static BigNum Min(int a, BigNum b)
        {
            return a < b ? new BigNum(a) : new BigNum(b);
        }
        
        //! Min comparator for BigNum
        /*!
         * \param a a BigNum to compare
         * \param b an int to compare
         * \return the smallest number between a and b
         */
        public static BigNum Min(BigNum a, int b)
        {
            return a < b ? new BigNum(a) : new BigNum(b);
        }
        
        #endregion

        #endregion

        #region OPERATIONS

        #region ADDITION

        //! Operator + for BigNums
        /*!
         * This fonction is using the method taught in low grade classes
         * \param a the first BigNum
         * \param b the second BigNum
         * \return a + b
         */
        public static BigNum operator +(BigNum a, BigNum b)
        {
            if (a == BigNumZero) return new BigNum(b);
            if (b == BigNumZero) return new BigNum(a);

            if (!a.IsNeg() && b.IsNeg()) return new BigNum(a - b.Abs());
            if (a.IsNeg() && !b.IsNeg()) return new BigNum(b - a.Abs());
            if (a.IsNeg() && b.IsNeg()) return new BigNum(-(a.Abs() + b.Abs()));

            var aCount = a.GetNumDigits();
            var bCount = b.GetNumDigits();

            var result = new BigNum(BigNumZero);
            var reminder = 0;
            int digit;

            if (aCount > bCount)
            {
                for (var i = 0; i < aCount; i++)
                {
                    var aDigit = i < aCount ? a.GetDigit(i) : 0;
                    var bDigit = i < bCount ? b.GetDigit(i) : 0;

                    digit = aDigit + bDigit + reminder;

                    if (digit > 9)
                    {
                        result.SetDigit(digit - 10, i);
                        reminder = 1;
                    }
                    else
                    {
                        result.SetDigit(digit, i);
                        reminder = 0;
                    }
                }

                if (reminder != 0) result.SetDigit(reminder, aCount);
            }

            else if (bCount > aCount)
            {
                return b + a;
            }

            else
            {
                for (var i = 0; i < aCount; i++)
                {
                    var sum = a.GetDigit(i) + b.GetDigit(i) + reminder;
                    digit = sum % 10;
                    result.SetDigit(digit, i);
                    reminder = sum / 10;
                }

                if (reminder != 0) result.SetDigit(reminder, aCount);
            }

            result.RemoveLeadingZeroes();
            return result;
        }

        //! Operator + for BigNum and int
        /*!
         * \param a BigNum
         * \param b int
         * \return a + b
         */
        public static BigNum operator +(BigNum a, int b)
        {
            return a + new BigNum(b);
        }

        //! Operator + for BigNum and int
        /*!
         * \param a int
         * \param b BigNum
         * \return a + b
         */
        public static BigNum operator +(int a, BigNum b)
        {
            return new BigNum(a) + b;
        }

        #endregion

        #region NEGATION

        //! Operator - for BigNum
        /*!
         * \param a the BigNum to be negated
         * \return -a
         */
        public static BigNum operator -(BigNum number)
        {
            var result = new BigNum(number);
            if (result == BigNumZero) return result;
            if (result.IsNeg())
            {
                result.SetSign(false);
                return result;
            }

            result.SetSign(true);
            return result;
        }

        #endregion

        #region SUBSTRACTION

        //! Operator - for BigNums
        /*!
         * This fonction is using the method taught in low grade classes
         * \param a the first BigNum
         * \param b the second BigNum
         * \return a - b
         */
        public static BigNum operator -(BigNum a, BigNum b)
        {
            if (a == BigNumZero) return new BigNum(-b);
            if (b == BigNumZero) return new BigNum(a);

            if (!a.IsNeg() && b.IsNeg()) return new BigNum(a + b.Abs());
            if (a.IsNeg() && !b.IsNeg()) return new BigNum(-(a.Abs() + b));
            if (a.IsNeg() && b.IsNeg()) return new BigNum(b.Abs() - a.Abs());

            var aCount = a.GetNumDigits();
            var bCount = b.GetNumDigits();
            var result = new BigNum(BigNumZero);

            if (a > b)
            {
                var reminder = 0;

                for (var i = 0; i < aCount; i++)
                {
                    var aDigit = i < aCount ? a.GetDigit(i) : 0;
                    var bDigit = i < bCount ? b.GetDigit(i) : 0;

                    var digit = aDigit - bDigit - reminder;

                    if (digit < 0)
                    {
                        result.SetDigit(digit + 10, i);
                        reminder = 1;
                    }
                    else
                    {
                        result.SetDigit(digit, i);
                        reminder = 0;
                    }
                }
            }

            else if (b > a)
            {
                return -(b - a);
            }

            return result;
        }

        //! Operator - for BigNum and int
        /*!
         * \param a BigNum
         * \param b int
         * \return a - b
         */
        public static BigNum operator -(BigNum a, int b)
        {
            return a - new BigNum(b);
        }

        //! Operator - for BigNum and int
        /*!
         * \param a int
         * \param b BigNum
         * \return a - b
         */
        public static BigNum operator -(int a, BigNum b)
        {
            return new BigNum(a) - b;
        }

        #endregion

        #region MULTIPLICATION

        //! Operator * for BigNums
        /*!
         * This fonction is using the method taught in low grade classes, but with a little bit of optimization usnig powers of 10
         * \param a the first BigNum
         * \param b the second BigNum
         * \return a * b
         */
        public static BigNum operator *(BigNum a, BigNum b)
        {
            if (a == BigNumZero || b == BigNumZero) return new BigNum(BigNumZero);
            if (a == BigNumOne) return new BigNum(b);
            if (b == BigNumOne) return new BigNum(a);

            if (a.IsNeg() && !b.IsNeg() || !a.IsNeg() && b.IsNeg()) return new BigNum(-(a.Abs() * b.Abs()));
            if (a.IsNeg() && b.IsNeg()) return new BigNum(a.Abs() * b.Abs());

            var result = new BigNum(BigNumZero);

            if (b < 10)
            {
                var bDigit = b.GetDigit(0);
                for (var i = 0; i < a.GetNumDigits(); i++) result += bDigit * a.GetDigit(i) * (int) Math.Pow(10, i);

                return result;
            }

            if (a < 10) return b * a;
            if (a < b) return b * a;

            var bCount = b.GetNumDigits();

            for (uint i = 0; i < bCount; i++) result += Pow10(a * b.GetDigit((int) i), i);

            return result;
        }

        //! Operator * for BigNum and int
        /*!
         * \param a BigNum
         * \param b int
         * \return a * b
         */
        public static BigNum operator *(BigNum a, int b)
        {
            return a * new BigNum(b);
        }

        //! Operator * for BigNum and int
        /*!
         * \param a int
         * \param b BigNum
         * \return a * b
         */
        public static BigNum operator *(int a, BigNum b)
        {
            return new BigNum(a) * b;
        }

        #endregion

        #region DIVISION

        //! Calculate the quotient and rest of a divided by b
        /*!
         * This fonction is using the division by repeated substraction method
         * \param a the nominator
         * \param b the denominator
         * \param rest [out] the BigNum in which the rest will be stored
         * \return the quotient
         */
        private static BigNum EuclideDivision(BigNum a, BigNum b, out BigNum rest)
        {
            var quotient = new BigNum(BigNumZero);
            if (b == BigNumZero) throw new DivideByZeroException();
            if (a == BigNumZero || a < b)
            {
                rest = new BigNum(a);
                return quotient;
            }

            rest = new BigNum(a);
            while (rest >= b)
            {
                quotient += 1;
                rest = rest - b;
            }

            return quotient;
        }

        //! Operator / for BigNums
        /*!
         * This fonction is using the division by repeated substraction method
         * \param a the first BigNum
         * \param b the second BigNum
         * \return a / b
         */
        public static BigNum operator /(BigNum a, BigNum b)
        {
            if (a.IsNeg() && !b.IsNeg() || !a.IsNeg() && b.IsNeg()) return -(a.Abs() / b.Abs());
            if (a.IsNeg() && b.IsNeg()) return a.Abs() / b.Abs();
            var quotient = EuclideDivision(a, b, out _);
            return quotient;
        }

        //! Operator / for BigNum and int
        /*!
         * \param a BigNum
         * \param b int
         * \return a / b
         */
        public static BigNum operator /(BigNum a, int b)
        {
            return a / new BigNum(b);
        }

        //! Operator / for BigNum and int
        /*!
         * \param a int
         * \param b BigNum
         * \return a / b
         */
        public static BigNum operator /(int a, BigNum b)
        {
            return new BigNum(a) / b;
        }

        #endregion

        #region MODULO

        //! Operator % for BigNums
        /*!
         * This fonction is using the division by repeated substraction method
         * \param a the first BigNum, has to be positive
         * \param b the second BigNum, has to be positive
         * \return a % b
         */
        public static BigNum operator %(BigNum a, BigNum b)
        {
            if (a.IsNeg() || b.IsNeg()) throw new SystemException("CPU melting");
            EuclideDivision(a, b, out var rest);
            return rest;
        }

        //! Operator % for BigNum and int
        /*!
         * \param a BigNum, has to be positive
         * \param b int, has to be positive
         * \return a % b
         */
        public static BigNum operator %(BigNum a, int b)
        {
            return a % new BigNum(b);
        }

        //! Operator % for BigNum and int
        /*!
         * \param a int, has to be positive
         * \param b BigNum, has to be positive
         * \return a % b
         */
        public static BigNum operator %(int a, BigNum b)
        {
            return new BigNum(a) % b;
        }

        #endregion

        #region xxCREMNT

        //! Operator ++ for BigNum
        /*!
         * \param number the BigNum
         * \return number++
         */
        public static BigNum operator ++(BigNum number)
        {
            return number + 1;
        }

        //! Operator -- for BigNum
        /*!
         * \param number the BigNum
         * \return number--
         */
        public static BigNum operator --(BigNum number)
        {
            return number - 1;
        }

        #endregion

        #endregion

        #endregion

        #region MATH

        #region SQRT

        //! Square root for BigNum, static version
        /*!
         * Finds the square root of the BigNum using a binary search algorithm
         * \param number the BigNum
         * \return the square root of the BigNum
         */
        public static BigNum Sqrt(BigNum number)
        {
            if (number.IsNeg()) throw new ArgumentException("number can't be negative", nameof(number));
            if (number == BigNumZero || number == BigNumOne) return new BigNum(number);
            
            var start = new BigNum(BigNumOne);
            var end = new BigNum(number);
            var result = new BigNum(BigNumZero);

            while (start <= end)
            {
                var mid = (start + end) / 2;

                if (mid * mid == number) return mid;

                if (mid * mid < number)
                {
                    start = mid + 1;
                    result = mid;
                }
                else
                {
                    end = mid - 1;
                }
            }

            return result;
        }

        //! Square root for BigNum, non-static version
        /*!
         * \return the square root of the BigNum
         */
        public BigNum Sqrt()
        {
            return Sqrt(this);
        }

        #endregion

        #region POW

        //! Power for BigNum, static version
        /*!
         * \param number the BigNum
         * \param pow a BigNum, the power which the number will be upped to
         * \return number to the power pow
         */
        public static BigNum Pow(BigNum number, BigNum pow)
        {
            if (pow.IsNeg()) return new BigNum(BigNumZero);

            if (pow == BigNumZero) return new BigNum(BigNumOne);

            if (pow == BigNumOne) return new BigNum(number);

            if (pow % 2 == BigNumZero) return Pow(number, pow / 2) * Pow(number, pow / 2);

            if (pow % 2 == BigNumOne) return number * Pow(number, pow / 2) * Pow(number, pow / 2);

            throw new SystemException("This shouldn't have happened...");
        }

        //! Power for BigNum and int, static version
        /*!
         * \param number the BigNum
         * \param pow an int, the power which the number will be upped to
         * \return number to the power pow
         */
        public static BigNum Pow(BigNum number, int pow)
        {
            return Pow(number, new BigNum(pow));
        }

        //! Power for BigNum and int, static version
        /*!
         * \param number an int
         * \param pow a BigNum, the power which the number will be upped to
         * \return number to the power pow
         */
        public static BigNum Pow(int number, BigNum pow)
        {
            return Pow(new BigNum(number), pow);
        }

        //! Power for BigNum and int, static version
        /*!
         * \param number an int
         * \param pow an int, the power which the number will be upped to
         * \return number to the power pow
         */
        public static BigNum Pow(int number, int pow)
        {
            return Pow(new BigNum(number), new BigNum(pow));
        }

        //! Power for BigNum, non-static version
        /*!
         * \param pow a BigNum, the power which the number will be upped to
         * \return the BigNum to the power pow
         */
        public BigNum Pow(BigNum pow)
        {
            return Pow(this, pow);
        }

        //! Power for BigNum and int, non-static version
        /*!
         * \param pow an int, the power which the number will be upped to
         * \return the BigNum to the power pow
         */
        public BigNum Pow(int pow)
        {
            return Pow(this, pow);
        }

        #endregion
        
        #region POW10
        
        //! Multiplies a BigNum by 10 to a certain power, static version
        /*!
         * \param number the BigNum to be upped
         * \param pow a uint, the power of ten the BigNum will be multiplied by
         * \return number times 10 to the power pow: number * (10 ^ pow)
         */
        public static BigNum Pow10(BigNum number, uint pow)
        {
            if (number == BigNumZero) return new BigNum(BigNumZero);
            if (number == BigNumOne) return new BigNum((int) Math.Pow(10, pow));
            if (pow == 0) return number;

            var result = new BigNum(BigNumZero);
            for (uint i = 0; i < number.GetNumDigits(); i++) result.SetDigit(number.GetDigit((int) i), (int) (i + pow));
            result.SetSign(number.IsNeg());
            return result;
        }

        //! Multiplies a BigNum by 10 to a certain power, non-static version
        /*!
         * \param pow a uint, the power of ten the BigNum will be multiplied by
         * \return number times 10 to the power pow: BigNum * (10 ^ pow)
         */
        public BigNum Pow10(uint pow)
        {
            return Pow10(this, pow);
        }

        #endregion

        #region ABS

        //! Get the Absolute value of a BigNum, static version
        /*!
         * \param number a BigNum, the number we want the absolute value of
         * \return the absolute value of number
         */
        public static BigNum Abs(BigNum number)
        {
            number.SetSign(false);
            return number;
        }

        //! Get the Absolute value of a BigNum, non-static version
        /*!
         * \return the absolute value of the BigNum
         */
        public BigNum Abs()
        {
            return Abs(this);
        }

        #endregion
        
        //TODO: docs
        #region FIBONACCI

        //! Calculate a term of the Fibonacci suite and return it as a BigNum
        /*!
         * \param rank a BigNum, the rank of the term you want
         * \return the term of the Fibonacci suite that corresponds to the rank
         */
        public static BigNum Fibonacci(BigNum rank)
        {
            if (rank < 0) throw new ArgumentException("term cannot be negative", nameof(rank));
            return rank < 2 ? new BigNum(rank) : Fibonacci(rank - 1) + Fibonacci(rank - 2);
        }

        //! Calculate a term of the Fibonacci suite and return it as a BigNum
        /*!
         * \param rank an int, the rank of the term you want
         * \return the term of the Fibonacci suite that corresponds to the rank
         */
        public static BigNum Fibonacci(int rank)
        {
            return Fibonacci(new BigNum(rank));
        }
        
        #endregion
        
        #region FACTORIAL

        //! Return the factorial of a BigNum, static version
        /*!
         * This function uses a recursive method
         * \param number the BigNum we want to get the factorial of
         * \return the factorial of number
         */
        public static BigNum Factorial(BigNum number)
        {
            if (number.IsNeg()) throw new ArgumentException("number cannot be negative", nameof(number));

            if (number > BigNumOne) return number * Factorial(number - 1);
            return new BigNum(BigNumOne);
        }

        //! Return the factorial of a BigNum, non-static version
        /*!
         * This function uses a recursive method
         * \return the factorial of the BigNum
         */
        public BigNum Factorial()
        {
            return Factorial(this);
        }

        #endregion
        
        #endregion
    }
}