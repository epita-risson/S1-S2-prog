﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace TinyBistro
{
    [TestFixture]
    public class Tests
    {
        [TestCase("123456", "123456", 0)]
        [TestCase("-123456", "-123456", 0)]
        [TestCase("", "", 1)]
        [TestCase("-0", "0", 0)]
        [TestCase("grere", "", 1)]
        //[Timeout(100)]
        public void TestBigNumCreation0(string input, string expected, int errors)
        {
            var err = 0;
            try
            {
                var n = new BigNum(input);
                Assert.AreEqual(expected, n.ToString());
            }
            catch (Exception e)
            {
                err++;
            }

            Assert.AreEqual(errors, err);
        }

        [TestCase("0", 1)]
        [TestCase("123456", 6)]
        [TestCase("-123456", 6)]
        //[Timeout(100)]
        public void TestGetNumDigits(string input, int expected)
        {
            var a = new BigNum(input);
            Assert.AreEqual(expected, a.GetNumDigits());
        }

        [TestCase("123456", false)]
        [TestCase("-1", true)]
        [TestCase("0", false)]
        [TestCase("-0", false)]
        //[Timeout(100)]
        public void TestIsNeg(string input, bool expected)
        {
            var a = new BigNum(input);
            Assert.AreEqual(expected, a.IsNeg());
        }

        [TestCase("123456", 7, 0, "7123456", 0)]
        [TestCase("123456", 42, 0, "1234567", 1)]
        [TestCase("-12", 7, 0, "-712", 0)]
        //[Timeout(100)]
        public void TestAddDigit(string input, int digit, int pos, string expected, int errors)
        {
            var err = 0;
            try
            {
                var a = new BigNum(input);
                a.AddDigit(digit);
                Assert.AreEqual(expected, a.ToString());
            }
            catch (Exception e)
            {
                err++;
            }

            Assert.AreEqual(errors, err);
        }

        [TestCase("123456", 7, 0, 6, 0)]
        [TestCase("-123456", 42, 0, 6, 0)]
        [TestCase("-12", 7, 42, 0, 1)]
        //[Timeout(100)]
        public void TestGetDigit(string input, int digit, int pos, int expected, int errors)
        {
            var err = 0;
            try
            {
                var a = new BigNum(input);
                Assert.AreEqual(expected, a.GetDigit(pos));
            }
            catch (Exception e)
            {
                err++;
            }

            Assert.AreEqual(errors, err);
        }

        [TestCase("123456", 7, 0, "123457", 0)]
        [TestCase("123456", 42, 0, "1234567", 1)]
        [TestCase("-12", 7, 5, "-700012", 0)]
        [TestCase("1", 4, -3, "", 1)]
        //[Timeout(100)]
        public void TestSetDigit(string input, int digit, int pos, string expected, int errors)
        {
            var err = 0;
            try
            {
                var a = new BigNum(input);
                a.SetDigit(digit, pos);
                Assert.AreEqual(expected, a.ToString());
            }
            catch (Exception e)
            {
                err++;
            }

            Assert.AreEqual(errors, err);
        }

        [TestCase("123456", "123456")]
        [TestCase("-12", "-12")]
        [TestCase("-00012", "-12")]
        [TestCase("000123456", "123456")]
        //[Timeout(100)]
        public void TestRemoveLeadingZeroes(string input, string expected)
        {
            var a = new BigNum(input);
            a.RemoveLeadingZeroes();
            Assert.AreEqual(expected, a.ToString());
        }

        [TestCase(1, 2, true)]
        [TestCase(-1, 2, true)]
        [TestCase(0, 0, false)]
        [TestCase(123456, 234567, true)]
        [TestCase(234567, 123456, false)]
        [TestCase(-123456, -234567, false)]
        [TestCase(-234567, -123456, true)]
        [TestCase(-12, -12, false)]
        //[Timeout(100)]
        public void TestInferior(int a, int b, bool expected)
        {
            var aa = new BigNum(a);
            var bb = new BigNum(b);
            Assert.AreEqual(expected, aa < bb);
        }

        [TestCase(1, 2, true)]
        [TestCase(-1, 2, true)]
        [TestCase(0, 0, false)]
        [TestCase(123456, 234567, true)]
        [TestCase(234567, 123456, false)]
        [TestCase(-123456, -234567, false)]
        [TestCase(-234567, -123456, true)]
        [TestCase(-12, -12, false)]
        //[Timeout(100)]
        public void TestInterior1(int a, int b, bool expected)
        {
            var aa = new BigNum(a);
            Assert.AreEqual(expected, aa < b);
        }

        [TestCase(1, 2, true)]
        [TestCase(-1, 2, true)]
        [TestCase(0, 0, false)]
        [TestCase(123456, 234567, true)]
        [TestCase(234567, 123456, false)]
        [TestCase(-123456, -234567, false)]
        [TestCase(-234567, -123456, true)]
        [TestCase(-12, -12, false)]
        //[Timeout(100)]
        public void TestInterior2(int a, int b, bool expected)
        {
            var bb = new BigNum(b);
            Assert.AreEqual(expected, a < bb);
        }

        [TestCase(1, 2, false)]
        [TestCase(-1, 2, false)]
        [TestCase(0, 0, false)]
        [TestCase(123456, 234567, false)]
        [TestCase(234567, 123456, true)]
        [TestCase(-123456, -234567, true)]
        [TestCase(-234567, -123456, false)]
        [TestCase(-12, -12, false)]
        //[Timeout(100)]
        public void TestSuperior(int a, int b, bool expected)
        {
            var aa = new BigNum(a);
            var bb = new BigNum(b);
            Assert.AreEqual(expected, aa > bb);
        }

        [TestCase(1, 2, false)]
        [TestCase(-1, 2, false)]
        [TestCase(0, 0, false)]
        [TestCase(123456, 234567, false)]
        [TestCase(234567, 123456, true)]
        [TestCase(-123456, -234567, true)]
        [TestCase(-234567, -123456, false)]
        [TestCase(-12, -12, false)]
        //[Timeout(100)]
        public void TestSuperior1(int a, int b, bool expected)
        {
            var aa = new BigNum(a);
            Assert.AreEqual(expected, aa > b);
        }

        [TestCase(1, 2, false)]
        [TestCase(-1, 2, false)]
        [TestCase(0, 0, false)]
        [TestCase(123456, 234567, false)]
        [TestCase(234567, 123456, true)]
        [TestCase(-123456, -234567, true)]
        [TestCase(-234567, -123456, false)]
        [TestCase(-12, -12, false)]
        //[Timeout(100)]
        public void TestSuperior2(int a, int b, bool expected)
        {
            var bb = new BigNum(b);
            Assert.AreEqual(expected, a > bb);
        }

        [TestCase(1, 2, false)]
        [TestCase(-1, 2, false)]
        [TestCase(0, 0, true)]
        [TestCase(123456, 234567, false)]
        [TestCase(234567, 123456, false)]
        [TestCase(-123456, -234567, false)]
        [TestCase(-234567, -123456, false)]
        [TestCase(-12, -12, true)]
        [TestCase(123456, 123456, true)]
        //[Timeout(100)]
        public void TestEquals(int a, int b, bool expected)
        {
            var aa = new BigNum(a);
            var bb = new BigNum(b);
            Assert.AreEqual(expected, aa == bb);
        }

        [TestCase(1, 2, false)]
        [TestCase(-1, 2, false)]
        [TestCase(0, 0, true)]
        [TestCase(123456, 234567, false)]
        [TestCase(234567, 123456, false)]
        [TestCase(-123456, -234567, false)]
        [TestCase(-234567, -123456, false)]
        [TestCase(-12, -12, true)]
        [TestCase(123456, 123456, true)]
        //[Timeout(100)]
        public void TestEquals1(int a, int b, bool expected)
        {
            var aa = new BigNum(a);
            Assert.AreEqual(expected, aa == b);
        }

        [TestCase(1, 2, false)]
        [TestCase(-1, 2, false)]
        [TestCase(0, 0, true)]
        [TestCase(123456, 234567, false)]
        [TestCase(234567, 123456, false)]
        [TestCase(-123456, -234567, false)]
        [TestCase(-234567, -123456, false)]
        [TestCase(-12, -12, true)]
        [TestCase(123456, 123456, true)]
        //[Timeout(100)]
        public void TestEquals2(int a, int b, bool expected)
        {
            var bb = new BigNum(b);
            Assert.AreEqual(expected, a == bb);
        }

        [TestCase(1, 2, false)]
        [TestCase(-1, 2, false)]
        [TestCase(0, 0, true)]
        [TestCase(123456, 234567, false)]
        [TestCase(234567, 123456, false)]
        [TestCase(-123456, -234567, false)]
        [TestCase(-234567, -123456, false)]
        [TestCase(-12, -12, true)]
        [TestCase(123456, 123456, true)]
        //[Timeout(100)]
        public void TestEquals4(int a, int b, bool expected)
        {
            var aa = new BigNum(a);
            var bb = new BigNum(b);
            Assert.AreEqual(expected, aa.Equals(bb));
        }

        [TestCase(1, 2, false)]
        [TestCase(-1, 2, false)]
        [TestCase(0, 0, true)]
        [TestCase(123456, 234567, false)]
        [TestCase(234567, 123456, false)]
        [TestCase(-123456, -234567, false)]
        [TestCase(-234567, -123456, false)]
        [TestCase(-12, -12, true)]
        [TestCase(123456, 123456, true)]
        //[Timeout(100)]
        public void TestEquals5(int a, int b, bool expected)
        {
            var aa = new BigNum(a);
            Assert.AreEqual(expected, aa.Equals(b));
        }

        [TestCase(1, 2, !false)]
        [TestCase(-1, 2, !false)]
        [TestCase(0, 0, !true)]
        [TestCase(123456, 234567, !false)]
        [TestCase(234567, 123456, !false)]
        [TestCase(-123456, -234567, !false)]
        [TestCase(-234567, -123456, !false)]
        [TestCase(-12, -12, !true)]
        [TestCase(123456, 123456, !true)]
        //[Timeout(100)]
        public void TestDifferent(int a, int b, bool expected)
        {
            var aa = new BigNum(a);
            var bb = new BigNum(b);
            Assert.AreEqual(expected, aa != bb);
        }

        [TestCase(1, 2, !false)]
        [TestCase(-1, 2, !false)]
        [TestCase(0, 0, !true)]
        [TestCase(123456, 234567, !false)]
        [TestCase(234567, 123456, !false)]
        [TestCase(-123456, -234567, !false)]
        [TestCase(-234567, -123456, !false)]
        [TestCase(-12, -12, !true)]
        [TestCase(123456, 123456, !true)]
        //[Timeout(100)]
        public void TestDifferent1(int a, int b, bool expected)
        {
            var aa = new BigNum(a);
            Assert.AreEqual(expected, aa != b);
        }

        [TestCase(1, 2, !false)]
        [TestCase(-1, 2, !false)]
        [TestCase(0, 0, !true)]
        [TestCase(123456, 234567, !false)]
        [TestCase(234567, 123456, !false)]
        [TestCase(-123456, -234567, !false)]
        [TestCase(-234567, -123456, !false)]
        [TestCase(-12, -12, !true)]
        [TestCase(123456, 123456, !true)]
        //[Timeout(100)]
        public void TestDifferent2(int a, int b, bool expected)
        {
            var bb = new BigNum(b);
            Assert.AreEqual(expected, a != bb);
        }

        [TestCase(1, 2, true)]
        [TestCase(-1, 2, true)]
        [TestCase(0, 0, true)]
        [TestCase(123456, 234567, true)]
        [TestCase(234567, 123456, false)]
        [TestCase(-123456, -234567, false)]
        [TestCase(-234567, -123456, true)]
        [TestCase(-12, -12, true)]
        //[Timeout(100)]
        public void TestInferiorEqual(int a, int b, bool expected)
        {
            var aa = new BigNum(a);
            var bb = new BigNum(b);
            Assert.AreEqual(expected, aa <= bb);
        }

        [TestCase(1, 2, true)]
        [TestCase(-1, 2, true)]
        [TestCase(0, 0, true)]
        [TestCase(123456, 234567, true)]
        [TestCase(234567, 123456, false)]
        [TestCase(-123456, -234567, false)]
        [TestCase(-234567, -123456, true)]
        [TestCase(-12, -12, true)]
        //[Timeout(100)]
        public void TestInteriorEqual1(int a, int b, bool expected)
        {
            var aa = new BigNum(a);
            Assert.AreEqual(expected, aa <= b);
        }

        [TestCase(1, 2, true)]
        [TestCase(-1, 2, true)]
        [TestCase(0, 0, true)]
        [TestCase(123456, 234567, true)]
        [TestCase(234567, 123456, false)]
        [TestCase(-123456, -234567, false)]
        [TestCase(-234567, -123456, true)]
        [TestCase(-12, -12, true)]
        //[Timeout(100)]
        public void TestInteriorEqual2(int a, int b, bool expected)
        {
            var bb = new BigNum(b);
            Assert.AreEqual(expected, a <= bb);
        }

        [TestCase(1, 2, false)]
        [TestCase(-1, 2, false)]
        [TestCase(0, 0, true)]
        [TestCase(123456, 234567, false)]
        [TestCase(234567, 123456, true)]
        [TestCase(-123456, -234567, true)]
        [TestCase(-234567, -123456, false)]
        [TestCase(-12, -12, true)]
        //[Timeout(100)]
        public void TestSuperiorEqual(int a, int b, bool expected)
        {
            var aa = new BigNum(a);
            var bb = new BigNum(b);
            Assert.AreEqual(expected, aa >= bb);
        }

        [TestCase(1, 2, false)]
        [TestCase(-1, 2, false)]
        [TestCase(0, 0, true)]
        [TestCase(123456, 234567, false)]
        [TestCase(234567, 123456, true)]
        [TestCase(-123456, -234567, true)]
        [TestCase(-234567, -123456, false)]
        [TestCase(-12, -12, true)]
        //[Timeout(100)]
        public void TestSuperiorEqual1(int a, int b, bool expected)
        {
            var aa = new BigNum(a);
            Assert.AreEqual(expected, aa >= b);
        }

        [TestCase(1, 2, false)]
        [TestCase(-1, 2, false)]
        [TestCase(0, 0, true)]
        [TestCase(123456, 234567, false)]
        [TestCase(234567, 123456, true)]
        [TestCase(-123456, -234567, true)]
        [TestCase(-234567, -123456, false)]
        [TestCase(-12, -12, true)]
        //[Timeout(100)]
        public void TestSuperiorEqual2(int a, int b, bool expected)
        {
            var bb = new BigNum(b);
            Assert.AreEqual(expected, a >= bb);
        }

        [TestCase(123456, 123456, "246912")]
        [TestCase(-123456, 123456, "0")]
        [TestCase(123456, 12, "123468")]
        [TestCase(12, 123456, "123468")]
        [TestCase(0, 123, "123")]
        [TestCase(-12, -2, "-14")]
        [TestCase(12, -2, "10")]
        [TestCase(99, 1, "100")]
        //[Timeout(100)]
        public void TestAddition0(int a, int b, string expected)
        {
            var aa = new BigNum(a);
            var bb = new BigNum(b);
            Assert.AreEqual(expected, (aa + bb).ToString());
        }

        [TestCase(123456, 123456, "246912")]
        [TestCase(-123456, 123456, "0")]
        [TestCase(123456, 12, "123468")]
        [TestCase(12, 123456, "123468")]
        [TestCase(0, 123, "123")]
        [TestCase(-12, -2, "-14")]
        [TestCase(12, -2, "10")]
        //[Timeout(100)]
        public void TestAddition1(int a, int b, string expected)
        {
            var aa = new BigNum(a);
            Assert.AreEqual(expected, (aa + b).ToString());
        }

        [TestCase(123456, 123456, "246912")]
        [TestCase(-123456, 123456, "0")]
        [TestCase(123456, 12, "123468")]
        [TestCase(12, 123456, "123468")]
        [TestCase(0, 123, "123")]
        [TestCase(-12, -2, "-14")]
        [TestCase(12, -2, "10")]
        [TestCase(1000, 270, "1270")]
        //[Timeout(100)]
        public void TestAddition2(int a, int b, string expected)
        {
            var bb = new BigNum(b);
            Assert.AreEqual(expected, (a + bb).ToString());
        }

        [TestCase(123456, 123456, "0")]
        [TestCase(-123456, 123456, "-246912")]
        [TestCase(123456, 12, "123444")]
        [TestCase(12, 123456, "-123444")]
        [TestCase(0, 123, "-123")]
        [TestCase(-12, -2, "-10")]
        [TestCase(12, -2, "14")]
        [TestCase(12301, 12, "12289")]
        //[Timeout(100)]
        public void TestSubstraction0(int a, int b, string expected)
        {
            var aa = new BigNum(a);
            var bb = new BigNum(b);
            Assert.AreEqual(expected, (aa - bb).ToString());
        }

        [TestCase(123456, 123456, "0")]
        [TestCase(-123456, 123456, "-246912")]
        [TestCase(123456, 12, "123444")]
        [TestCase(12, 123456, "-123444")]
        [TestCase(0, 123, "-123")]
        [TestCase(-12, -2, "-10")]
        [TestCase(12, -2, "14")]
        //[Timeout(100)]
        public void TestSubstraction1(int a, int b, string expected)
        {
            var aa = new BigNum(a);
            Assert.AreEqual(expected, (aa - b).ToString());
        }

        [TestCase(123456, 123456, "0")]
        [TestCase(-123456, 123456, "-246912")]
        [TestCase(123456, 12, "123444")]
        [TestCase(12, 123456, "-123444")]
        [TestCase(0, 123, "-123")]
        [TestCase(-12, -2, "-10")]
        [TestCase(12, -2, "14")]
        //[Timeout(100)]
        public void TestSubstraction2(int a, int b, string expected)
        {
            var bb = new BigNum(b);
            Assert.AreEqual(expected, (a - bb).ToString());
        }

        [TestCase(0, "0")]
        [TestCase(1, "-1")]
        [TestCase(-1, "1")]
        //[Timeout(100)]
        public void TestNegation(int a, string expected)
        {
            var aa = new BigNum(a);
            Assert.AreEqual(expected, (-aa).ToString());
        }

        [TestCase(12, 13, "156")]
        [TestCase(-12, 13, "-156")]
        [TestCase(12345, 12, "148140")]
        [TestCase(12, 12345, "148140")]
        [TestCase(0, 123, "0")]
        [TestCase(-12, -2, "24")]
        [TestCase(12, -2, "-24")]
        //[Timeout(100)]
        public void TestMultiplication0(int a, int b, string expected)
        {
            var aa = new BigNum(a);
            var bb = new BigNum(b);
            Assert.AreEqual(expected, (aa * bb).ToString());
        }

        [TestCase(12, 13, "156")]
        [TestCase(-12, 13, "-156")]
        [TestCase(12345, 12, "148140")]
        [TestCase(12, 12345, "148140")]
        [TestCase(0, 123, "0")]
        [TestCase(-12, -2, "24")]
        [TestCase(12, -2, "-24")]
        //[Timeout(100)]
        public void TestMultiplication1(int a, int b, string expected)
        {
            var aa = new BigNum(a);
            Assert.AreEqual(expected, (aa * b).ToString());
        }

        [TestCase(12, 13, "156")]
        [TestCase(-12, 13, "-156")]
        [TestCase(12345, 12, "148140")]
        [TestCase(12, 12345, "148140")]
        [TestCase(0, 123, "0")]
        [TestCase(-12, -2, "24")]
        [TestCase(12, -2, "-24")]
        //[Timeout(100)]
        public void TestMultiplication2(int a, int b, string expected)
        {
            var bb = new BigNum(b);
            Assert.AreEqual(expected, (a * bb).ToString());
        }

        [TestCase(12, 13, "0", 0)]
        [TestCase(-12, 13, "0", 0)]
        [TestCase(12345, 13, "949", 0)]
        [TestCase(12, 12345, "0", 0)]
        [TestCase(123, 0, "0", 1)]
        [TestCase(-12, -2, "6", 0)]
        [TestCase(12, -2, "-6", 0)]
        [TestCase(15, 4, "3", 0)]
        //[Timeout(100)]
        public void TestDivision0(int a, int b, string expected, int errors)
        {
            var err = 0;
            try
            {
                var aa = new BigNum(a);
                var bb = new BigNum(b);
                Assert.AreEqual(expected, (aa / bb).ToString());
            }
            catch (Exception e)
            {
                err++;
            }

            Assert.AreEqual(errors, err);
        }

        [TestCase(12, 13, "0", 0)]
        [TestCase(-12, 13, "0", 0)]
        [TestCase(12345, 12, "1028", 0)]
        [TestCase(12, 12345, "0", 0)]
        [TestCase(123, 0, "0", 1)]
        [TestCase(-12, -2, "6", 0)]
        [TestCase(12, -2, "-6", 0)]
        //[Timeout(100)]
        public void TestDivision1(int a, int b, string expected, int errors)
        {
            var err = 0;
            try
            {
                var aa = new BigNum(a);
                Assert.AreEqual(expected, (aa / b).ToString());
            }
            catch (Exception e)
            {
                err++;
            }

            Assert.AreEqual(errors, err);
        }

        [TestCase(12, 13, "0", 0)]
        [TestCase(-12, 13, "0", 0)]
        [TestCase(12345, 12, "1028", 0)]
        [TestCase(12, 12345, "0", 0)]
        [TestCase(123, 0, "0", 1)]
        [TestCase(-12, -2, "6", 0)]
        [TestCase(12, -2, "-6", 0)]
        //[Timeout(100)]
        public void TestDivision2(int a, int b, string expected, int errors)
        {
            var err = 0;
            try
            {
                var bb = new BigNum(b);
                Assert.AreEqual(expected, (a / bb).ToString());
            }
            catch (Exception e)
            {
                err++;
            }

            Assert.AreEqual(errors, err);
        }

        [TestCase(12, 2, "0", 0)]
        [TestCase(12, 5, "2", 0)]
        [TestCase(5, 12, "5", 0)]
        [TestCase(-2, 2, "", 1)]
        [TestCase(2, -2, "", 1)]
        //[Timeout(100)]
        public void TestModulo0(int a, int b, string expected, int errors)
        {
            var err = 0;
            try
            {
                var aa = new BigNum(a);
                var bb = new BigNum(b);
                Assert.AreEqual(expected, (aa % bb).ToString());
            }
            catch (Exception e)
            {
                err++;
            }

            Assert.AreEqual(errors, err);
        }

        [TestCase(12, 2, "0", 0)]
        [TestCase(12, 5, "2", 0)]
        [TestCase(5, 12, "5", 0)]
        [TestCase(-2, 2, "", 1)]
        [TestCase(2, -2, "", 1)]
        //[Timeout(100)]
        public void TestModulo1(int a, int b, string expected, int errors)
        {
            var err = 0;
            try
            {
                var aa = new BigNum(a);
                Assert.AreEqual(expected, (aa % b).ToString());
            }
            catch (Exception e)
            {
                err++;
            }

            Assert.AreEqual(errors, err);
        }

        [TestCase(12, 2, "0", 0)]
        [TestCase(12, 5, "2", 0)]
        [TestCase(5, 12, "5", 0)]
        [TestCase(-2, 2, "", 1)]
        [TestCase(2, -2, "", 1)]
        //[Timeout(100)]
        public void TestModulo2(int a, int b, string expected, int errors)
        {
            var err = 0;
            try
            {
                var bb = new BigNum(b);
                Assert.AreEqual(expected, (a % bb).ToString());
            }
            catch (Exception e)
            {
                err++;
            }

            Assert.AreEqual(errors, err);
        }

        [TestCase(123, "124")]
        [TestCase(-123, "-122")]
        //[Timeout(100)]
        public void TestIncrement(int a, string expected)
        {
            var aa = new BigNum(a);
            Assert.AreEqual(expected, (++aa).ToString());
        }

        [TestCase(123, "122")]
        [TestCase(-123, "-124")]
        //[Timeout(100)]
        public void TestDecrement(int a, string expected)
        {
            var aa = new BigNum(a);
            Assert.AreEqual(expected, (--aa).ToString());
        }

        [TestCase(16, "4", 0)]
        [TestCase(25, "5", 0)]
        [TestCase(1, "1", 0)]
        [TestCase(-42, "", 1)]
        [TestCase(0, "0", 0)]
        //[Timeout(100)]
        public void TestSqrt(int a, string expected, int errors)
        {
            var err = 0;
            try
            {
                var aa = new BigNum(a);
                Assert.AreEqual(expected, aa.Sqrt().ToString());
            }
            catch (Exception e)
            {
                err++;
            }

            Assert.AreEqual(errors, err);
        }

        [TestCase(12345, 2, "152399025", 0)]
        [TestCase(3, 3, "27", 0)]
        [TestCase(1654, 0, "1", 0)]
        [TestCase(1136456, 1, "1136456", 0)]
        [TestCase(42, -17, "0", 0)]
        [TestCase(17, 6, "24137569", 0)]
        //[Timeout(100)]
        public void TestPow(int a, int pow, string expected, int errors)
        {
            var err = 0;
            try
            {
                var aa = new BigNum(a);
                var power = new BigNum(pow);
                Assert.AreEqual(expected, BigNum.Pow(aa, power).ToString());
            }
            catch (Exception e)
            {
                err++;
                Console.WriteLine(e.ToString());
            }

            Assert.AreEqual(errors, err);
        }
        
        [TestCase(12345, (uint) 2, "1234500")]
        [TestCase(-123456, (uint) 5, "-12345600000")]
        [TestCase(0, (uint) 3, "0")]
        [TestCase(1, (uint) 3, "1000")]
        [TestCase(1556, (uint) 0, "1556")]
        //[Timeout(100)]
        public void TestPow10(int number, uint pow, string expected)
        {
            var a = new BigNum(number);
            Assert.AreEqual(expected, a.Pow10(pow).ToString());
        }
        
        [TestCase(-1568)]
        [TestCase(0)]
        [TestCase(1848)]
        //[Timeout(100)]
        public void TestAbs(int input)
        {
            var a = new BigNum(input);
            Assert.AreEqual(false, a.Abs().IsNeg());
        }

        [Test]
        //[Timeout(100)]
        public void TestBigNumCreation1()
        {
            var n = new BigNum(123456);
            Assert.AreEqual("123456", n.ToString());
        }

        [Test]
        //[Timeout(100)]
        public void TestBigNumCreation2()
        {
            var n = new BigNum(new BigNum("123456"));
            Assert.AreEqual("123456", n.ToString());
        }

        [Test]
        //[Timeout(100)]
        public void TestBigNumCreation3()
        {
            var a = new List<uint> {6, 5, 4, 3, 2, 1};
            var n = new BigNum(a, true);
            Assert.AreEqual("-123456", n.ToString());
        }

        [TestCase(12345, 1)]
        [TestCase(0, 0)]
        [TestCase(-0, 0)]
        [TestCase(-12345, -1)]
        //[Timeout(100)]
        public void TestSign(int n, int expected)
        {
            var a = new BigNum(n);
            Assert.AreEqual(expected, a.Sign());
        }

        [TestCase(12345, 123, "12345")]
        [TestCase(0, 0, "0")]
        [TestCase(-123, 12, "12")]
        //[Timeout(100)]
        public void TestMax(int a, int b, string expected)
        {
            var aa = new BigNum(a);
            var bb = new BigNum(b);
            Assert.AreEqual(expected, BigNum.Max(aa, bb).ToString());
        }
        
        [TestCase(12345, 123, "123")]
        [TestCase(0, 0, "0")]
        [TestCase(-123, 12, "-123")]
        //[Timeout(100)]
        public void TestMin(int a, int b, string expected)
        {
            var aa = new BigNum(a);
            var bb = new BigNum(b);
            Assert.AreEqual(expected, BigNum.Min(aa, bb).ToString());
        }

        [TestCase(14, "61408487424", 0)]
        [TestCase(0, "1", 0)]
        [TestCase(1, "1", 0)]
        [TestCase(6, "720", 0)]
        [TestCase(-42, "", 1)]
        [TestCase(20, "23279247360", 0)]
        //[Timeout(100)]
        public void TestFactorial(int a, string expected, int errors)
        {
            var err = 0;
            try
            {
                var aa = new BigNum(a);
                Assert.AreEqual(expected, aa.Factorial().ToString());
            }
            catch (Exception e)
            {
                err++;
            }
            Assert.AreEqual(err, errors);
        }

        [TestCase(0, "0", 0)]
        [TestCase(1, "1", 0)]
        [TestCase(2, "1", 0)]
        [TestCase(3, "2", 0)]
        [TestCase(-1, "", 1)]
        [TestCase(10, "55", 0)]
        //[Timeout(100)]
        public void TestFibonacci(int a, string expected, int errors)
        {
            var err = 0;
            try
            {
                var aa = new BigNum(a);
                Assert.AreEqual(expected, BigNum.Fibonacci(aa).ToString());
            }
            catch (Exception e)
            {
                err++;
            }
            Assert.AreEqual(errors, err);
        }
    }
}