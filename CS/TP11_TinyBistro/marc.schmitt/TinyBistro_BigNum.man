.TH "TinyBistro.BigNum" 3 "Thu Mar 29 2018" "TinyBistro" \" -*- nroff -*-
.ad l
.nh
.SH NAME
TinyBistro.BigNum
.br
A list of all bonus functions is available at the end of the file
.SH SYNOPSIS
.br
.PP
.SS "Public Member Functions"

.in +1c
.ti -1c
.RI "\fBBigNum\fP (string number)"
.br
.RI "Constructor using a string\&. "
.ti -1c
.RI "\fBBigNum\fP (List< uint > number, bool sign=false)"
.br
.RI "Constructor using a list of digits and the sign of the number\&. "
.ti -1c
.RI "\fBBigNum\fP (\fBBigNum\fP number)"
.br
.RI "Constructor using a \fBBigNum\fP object\&. "
.ti -1c
.RI "\fBBigNum\fP (int n)"
.br
.RI "Constructor using an int\&. "
.ti -1c
.RI "int \fBGetNumDigits\fP ()"
.br
.RI "Return the number of digits in the number\&. "
.ti -1c
.RI "bool \fBIsNeg\fP ()"
.br
.RI "Check if the number is negative\&. "
.ti -1c
.RI "\fBBigNum\fP \fBGetBigNumDigits\fP ()"
.br
.RI "Return the \fBBigNum\fP of digits in the number\&. "
.ti -1c
.RI "new string \fBToString\fP ()"
.br
.RI "Create a string representation of a \fBBigNum\fP, non-static version\&. "
.ti -1c
.RI "void \fBPrint\fP ()"
.br
.RI "Print a \fBBigNum\fP\&. "
.ti -1c
.RI "int \fBSign\fP ()"
.br
.RI "Return an integer that indicates the sign of a \fBBigNum\fP, non-static version\&. "
.ti -1c
.RI "void \fBAddDigit\fP (int digit)"
.br
.RI "Add a digit in front of the number (i\&.e\&. AddDigit(0) doesn't do anything) "
.ti -1c
.RI "int \fBGetDigit\fP (int position)"
.br
.RI "Get the digit at a certain position\&. "
.ti -1c
.RI "void \fBSetDigit\fP (int digit, int position)"
.br
.RI "Set a digit of the number\&. "
.ti -1c
.RI "void \fBRemoveLeadingZeroes\fP ()"
.br
.RI "Remove useless zeroes in front of a \fBBigNum\fP\&. "
.ti -1c
.RI "virtual bool \fBEquals\fP (\fBBigNum\fP number)"
.br
.RI "Check if two BigNums are equal\&. "
.ti -1c
.RI "virtual bool \fBEquals\fP (int number)"
.br
.RI "Check if a \fBBigNum\fP is equal to an int\&. "
.ti -1c
.RI "\fBBigNum\fP \fBSqrt\fP ()"
.br
.RI "Square root for \fBBigNum\fP, non-static version\&. "
.ti -1c
.RI "\fBBigNum\fP \fBPow\fP (\fBBigNum\fP pow)"
.br
.RI "Power for \fBBigNum\fP, non-static version\&. "
.ti -1c
.RI "\fBBigNum\fP \fBPow\fP (int pow)"
.br
.RI "Power for \fBBigNum\fP and int, non-static version\&. "
.ti -1c
.RI "\fBBigNum\fP \fBPow10\fP (uint pow)"
.br
.RI "Multiplies a \fBBigNum\fP by 10 to a certain power, non-static version\&. "
.ti -1c
.RI "\fBBigNum\fP \fBAbs\fP ()"
.br
.RI "Get the Absolute value of a \fBBigNum\fP, non-static version\&. "
.ti -1c
.RI "\fBBigNum\fP \fBFactorial\fP ()"
.br
.RI "Return the factorial of a \fBBigNum\fP, non-static version\&. "
.in -1c
.SS "Static Public Member Functions"

.in +1c
.ti -1c
.RI "static string \fBToString\fP (\fBBigNum\fP number)"
.br
.RI "Create a string representation of a \fBBigNum\fP, static version\&. "
.ti -1c
.RI "static int \fBSign\fP (\fBBigNum\fP number)"
.br
.RI "Return an integer that indicates the sign of a \fBBigNum\fP, non-static version\&. "
.ti -1c
.RI "static bool \fBoperator<\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "Comparator < for BigNums\&. "
.ti -1c
.RI "static bool \fBoperator<\fP (\fBBigNum\fP a, int b)"
.br
.RI "Comparator < for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static bool \fBoperator<\fP (int a, \fBBigNum\fP b)"
.br
.RI "Comparator < for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static bool \fBoperator>\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "Comparator > for BigNums\&. "
.ti -1c
.RI "static bool \fBoperator>\fP (\fBBigNum\fP a, int b)"
.br
.RI "Comparator > for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static bool \fBoperator>\fP (int a, \fBBigNum\fP b)"
.br
.RI "Comparator > for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static bool \fBoperator==\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "Comparator == for BigNums\&. "
.ti -1c
.RI "static bool \fBoperator==\fP (\fBBigNum\fP a, int b)"
.br
.RI "Comparator == for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static bool \fBoperator==\fP (int a, \fBBigNum\fP b)"
.br
.RI "Comparator == for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static bool \fBoperator!=\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "Comparator != for BigNums\&. "
.ti -1c
.RI "static bool \fBoperator!=\fP (\fBBigNum\fP a, int b)"
.br
.RI "Comparator != for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static bool \fBoperator!=\fP (int a, \fBBigNum\fP b)"
.br
.RI "Comparator != for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static bool \fBoperator<=\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "Comparator <= for BigNums\&. "
.ti -1c
.RI "static bool \fBoperator<=\fP (\fBBigNum\fP a, int b)"
.br
.RI "Comparator <= for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static bool \fBoperator<=\fP (int a, \fBBigNum\fP b)"
.br
.RI "Comparator <= for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static bool \fBoperator>=\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "Comparator >= for BigNums\&. "
.ti -1c
.RI "static bool \fBoperator>=\fP (\fBBigNum\fP a, int b)"
.br
.RI "Comparator >= for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static bool \fBoperator>=\fP (int a, \fBBigNum\fP b)"
.br
.RI "Comparator >= for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBMax\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "Max comparator for \fBBigNum\fP\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBMax\fP (int a, \fBBigNum\fP b)"
.br
.RI "Max comparator for \fBBigNum\fP\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBMax\fP (\fBBigNum\fP a, int b)"
.br
.RI "Max comparator for \fBBigNum\fP\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBMin\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "Min comparator for \fBBigNum\fP\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBMin\fP (int a, \fBBigNum\fP b)"
.br
.RI "Min comparator for \fBBigNum\fP\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBMin\fP (\fBBigNum\fP a, int b)"
.br
.RI "Min comparator for \fBBigNum\fP\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator+\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "Operator + for BigNums\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator+\fP (\fBBigNum\fP a, int b)"
.br
.RI "Operator + for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator+\fP (int a, \fBBigNum\fP b)"
.br
.RI "Operator + for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator\-\fP (\fBBigNum\fP number)"
.br
.RI "Operator - for \fBBigNum\fP\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator\-\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "Operator - for BigNums\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator\-\fP (\fBBigNum\fP a, int b)"
.br
.RI "Operator - for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator\-\fP (int a, \fBBigNum\fP b)"
.br
.RI "Operator - for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator*\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "Operator * for BigNums\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator*\fP (\fBBigNum\fP a, int b)"
.br
.RI "Operator * for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator*\fP (int a, \fBBigNum\fP b)"
.br
.RI "Operator * for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator/\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "Operator / for BigNums\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator/\fP (\fBBigNum\fP a, int b)"
.br
.RI "Operator / for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator/\fP (int a, \fBBigNum\fP b)"
.br
.RI "Operator / for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator%\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "Operator % for BigNums\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator%\fP (\fBBigNum\fP a, int b)"
.br
.RI "Operator % for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator%\fP (int a, \fBBigNum\fP b)"
.br
.RI "Operator % for \fBBigNum\fP and int\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator++\fP (\fBBigNum\fP number)"
.br
.RI "Operator ++ for \fBBigNum\fP\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBoperator\-\-\fP (\fBBigNum\fP number)"
.br
.RI "Operator -- for \fBBigNum\fP\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBSqrt\fP (\fBBigNum\fP number)"
.br
.RI "Square root for \fBBigNum\fP, static version\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBPow\fP (\fBBigNum\fP number, \fBBigNum\fP pow)"
.br
.RI "Power for \fBBigNum\fP, static version\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBPow\fP (\fBBigNum\fP number, int pow)"
.br
.RI "Power for \fBBigNum\fP and int, static version\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBPow\fP (int number, \fBBigNum\fP pow)"
.br
.RI "Power for \fBBigNum\fP and int, static version\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBPow\fP (int number, int pow)"
.br
.RI "Power for \fBBigNum\fP and int, static version\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBPow10\fP (\fBBigNum\fP number, uint pow)"
.br
.RI "Multiplies a \fBBigNum\fP by 10 to a certain power, static version\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBAbs\fP (\fBBigNum\fP number)"
.br
.RI "Get the Absolute value of a \fBBigNum\fP, static version\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBFibonacci\fP (\fBBigNum\fP rank)"
.br
.RI "Calculate a term of the Fibonacci suite and return it as a \fBBigNum\fP\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBFibonacci\fP (int rank)"
.br
.RI "Calculate a term of the Fibonacci suite and return it as a \fBBigNum\fP\&. "
.ti -1c
.RI "static \fBBigNum\fP \fBFactorial\fP (\fBBigNum\fP number)"
.br
.RI "Return the factorial of a \fBBigNum\fP, static version\&. "
.in -1c
.SS "Static Public Attributes"

.in +1c
.ti -1c
.RI "static readonly \fBBigNum\fP \fBBigNumZero\fP = new \fBBigNum\fP(0)"
.br
.ti -1c
.RI "static readonly \fBBigNum\fP \fBBigNumOne\fP = new \fBBigNum\fP(1)"
.br
.in -1c
.SH "Detailed Description"
.PP 
The \fBBigNum\fP class 
.br
.PP
Use to manipulate very large integers (positive and negative)
.PP
The source code is categorized using #regions in order to improve readability
.PP
The Program\&.cs was NOT updated to take into consideration the bonuses implemented, so a Tests\&.cs file is provided, however, in order for the project to build, it was excluded from the project\&. To use said file, add it to the Project and install NUnit 3\&.10\&.1 using NuGet, then you can run all of the Unit Tests, and even add some !
.PP
\fBAuthor:\fP
.RS 4
marc\&.schmitt 
.RE
.PP

.SH "Constructor & Destructor Documentation"
.PP 
.SS "TinyBistro\&.BigNum\&.BigNum (string number)\fC [inline]\fP"

.PP
Constructor using a string\&. 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP a number represented by a string (e\&.g\&.: '123456', '-42') 
.RE
.PP

.SS "TinyBistro\&.BigNum\&.BigNum (List< uint > number, bool sign = \fCfalse\fP)\fC [inline]\fP"

.PP
Constructor using a list of digits and the sign of the number\&. 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP list of uint (in reversed order, i\&.e\&. 123 will be represented as [3, 2, 1]) 
.br
\fIsign\fP sign of the number(true for negative, default is false) 
.RE
.PP

.SS "TinyBistro\&.BigNum\&.BigNum (\fBBigNum\fP number)\fC [inline]\fP"

.PP
Constructor using a \fBBigNum\fP object\&. 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP a \fBBigNum\fP object, which will be copied 
.RE
.PP

.SS "TinyBistro\&.BigNum\&.BigNum (int n)\fC [inline]\fP"

.PP
Constructor using an int\&. 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP an integer number ,positive or negative 
.RE
.PP

.SH "Member Function Documentation"
.PP 
.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.Abs (\fBBigNum\fP number)\fC [inline]\fP, \fC [static]\fP"

.PP
Get the Absolute value of a \fBBigNum\fP, static version\&. 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP a \fBBigNum\fP, the number we want the absolute value of 
.RE
.PP
\fBReturns:\fP
.RS 4
the absolute value of number 
.RE
.PP

.SS "\fBBigNum\fP TinyBistro\&.BigNum\&.Abs ()\fC [inline]\fP"

.PP
Get the Absolute value of a \fBBigNum\fP, non-static version\&. 
.PP
\fBReturns:\fP
.RS 4
the absolute value of the \fBBigNum\fP 
.RE
.PP

.SS "void TinyBistro\&.BigNum\&.AddDigit (int digit)\fC [inline]\fP"

.PP
Add a digit in front of the number (i\&.e\&. AddDigit(0) doesn't do anything) 
.PP
\fBParameters:\fP
.RS 4
\fIdigit\fP the digit to add in front of the number 
.RE
.PP

.SS "virtual bool TinyBistro\&.BigNum\&.Equals (\fBBigNum\fP number)\fC [inline]\fP, \fC [virtual]\fP"

.PP
Check if two BigNums are equal\&. 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP the \fBBigNum\fP to compare with the current one 
.RE
.PP
\fBReturns:\fP
.RS 4
this == number 
.RE
.PP

.SS "virtual bool TinyBistro\&.BigNum\&.Equals (int number)\fC [inline]\fP, \fC [virtual]\fP"

.PP
Check if a \fBBigNum\fP is equal to an int\&. 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP the integer to compare with the \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
this == number 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.Factorial (\fBBigNum\fP number)\fC [inline]\fP, \fC [static]\fP"

.PP
Return the factorial of a \fBBigNum\fP, static version\&. This function uses a recursive method 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP the \fBBigNum\fP we want to get the factorial of 
.RE
.PP
\fBReturns:\fP
.RS 4
the factorial of number 
.RE
.PP

.SS "\fBBigNum\fP TinyBistro\&.BigNum\&.Factorial ()\fC [inline]\fP"

.PP
Return the factorial of a \fBBigNum\fP, non-static version\&. This function uses a recursive method 
.PP
\fBReturns:\fP
.RS 4
the factorial of the \fBBigNum\fP 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.Fibonacci (\fBBigNum\fP rank)\fC [inline]\fP, \fC [static]\fP"

.PP
Calculate a term of the Fibonacci suite and return it as a \fBBigNum\fP\&. 
.PP
\fBParameters:\fP
.RS 4
\fIrank\fP a \fBBigNum\fP, the rank of the term you want 
.RE
.PP
\fBReturns:\fP
.RS 4
the term of the Fibonacci suite that corresponds to the rank 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.Fibonacci (int rank)\fC [inline]\fP, \fC [static]\fP"

.PP
Calculate a term of the Fibonacci suite and return it as a \fBBigNum\fP\&. 
.PP
\fBParameters:\fP
.RS 4
\fIrank\fP an int, the rank of the term you want 
.RE
.PP
\fBReturns:\fP
.RS 4
the term of the Fibonacci suite that corresponds to the rank 
.RE
.PP

.SS "\fBBigNum\fP TinyBistro\&.BigNum\&.GetBigNumDigits ()\fC [inline]\fP"

.PP
Return the \fBBigNum\fP of digits in the number\&. 
.PP
\fBReturns:\fP
.RS 4
the \fBBigNum\fP of digits in the number 
.RE
.PP

.SS "int TinyBistro\&.BigNum\&.GetDigit (int position)\fC [inline]\fP"

.PP
Get the digit at a certain position\&. 
.PP
\fBParameters:\fP
.RS 4
\fIposition\fP the position of the digit to get 
.RE
.PP
\fBReturns:\fP
.RS 4
the digit at the position 
.RE
.PP

.SS "int TinyBistro\&.BigNum\&.GetNumDigits ()\fC [inline]\fP"

.PP
Return the number of digits in the number\&. 
.PP
\fBReturns:\fP
.RS 4
the number of digits in the number 
.RE
.PP

.SS "bool TinyBistro\&.BigNum\&.IsNeg ()\fC [inline]\fP"

.PP
Check if the number is negative\&. 
.PP
\fBReturns:\fP
.RS 4
true if the number is negative, false if positive or equal to zero 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.Max (\fBBigNum\fP a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Max comparator for \fBBigNum\fP\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP the first \fBBigNum\fP to compare 
.br
\fIb\fP the second \fBBigNum\fP to compare 
.RE
.PP
\fBReturns:\fP
.RS 4
the biggest \fBBigNum\fP between a and b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.Max (int a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Max comparator for \fBBigNum\fP\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP an int to compare 
.br
\fIb\fP a \fBBigNum\fP to compare 
.RE
.PP
\fBReturns:\fP
.RS 4
the biggest number between a and b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.Max (\fBBigNum\fP a, int b)\fC [inline]\fP, \fC [static]\fP"

.PP
Max comparator for \fBBigNum\fP\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP a \fBBigNum\fP to compare 
.br
\fIb\fP an int to compare 
.RE
.PP
\fBReturns:\fP
.RS 4
the biggest number between a and b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.Min (\fBBigNum\fP a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Min comparator for \fBBigNum\fP\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP the first \fBBigNum\fP to compare 
.br
\fIb\fP the second \fBBigNum\fP to compare 
.RE
.PP
\fBReturns:\fP
.RS 4
the smallest \fBBigNum\fP between a and b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.Min (int a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Min comparator for \fBBigNum\fP\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP an int to compare 
.br
\fIb\fP a \fBBigNum\fP to compare 
.RE
.PP
\fBReturns:\fP
.RS 4
the smallest number between a and b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.Min (\fBBigNum\fP a, int b)\fC [inline]\fP, \fC [static]\fP"

.PP
Min comparator for \fBBigNum\fP\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP a \fBBigNum\fP to compare 
.br
\fIb\fP an int to compare 
.RE
.PP
\fBReturns:\fP
.RS 4
the smallest number between a and b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator!= (\fBBigNum\fP a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator != for BigNums\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP first \fBBigNum\fP 
.br
\fIb\fP second \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a != b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator!= (\fBBigNum\fP a, int b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator != for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP \fBBigNum\fP 
.br
\fIb\fP int 
.RE
.PP
\fBReturns:\fP
.RS 4
a != b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator!= (int a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator != for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP int 
.br
\fIb\fP \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a != b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator% (\fBBigNum\fP a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator % for BigNums\&. This fonction is using the division by repeated substraction method 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP the first \fBBigNum\fP, has to be positive 
.br
\fIb\fP the second \fBBigNum\fP, has to be positive 
.RE
.PP
\fBReturns:\fP
.RS 4
a % b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator% (\fBBigNum\fP a, int b)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator % for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP \fBBigNum\fP, has to be positive 
.br
\fIb\fP int, has to be positive 
.RE
.PP
\fBReturns:\fP
.RS 4
a % b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator% (int a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator % for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP int, has to be positive 
.br
\fIb\fP \fBBigNum\fP, has to be positive 
.RE
.PP
\fBReturns:\fP
.RS 4
a % b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator* (\fBBigNum\fP a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator * for BigNums\&. This fonction is using the method taught in low grade classes, but with a little bit of optimization usnig powers of 10 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP the first \fBBigNum\fP 
.br
\fIb\fP the second \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a * b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator* (\fBBigNum\fP a, int b)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator * for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP \fBBigNum\fP 
.br
\fIb\fP int 
.RE
.PP
\fBReturns:\fP
.RS 4
a * b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator* (int a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator * for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP int 
.br
\fIb\fP \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a * b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator+ (\fBBigNum\fP a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator + for BigNums\&. This fonction is using the method taught in low grade classes 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP the first \fBBigNum\fP 
.br
\fIb\fP the second \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a + b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator+ (\fBBigNum\fP a, int b)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator + for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP \fBBigNum\fP 
.br
\fIb\fP int 
.RE
.PP
\fBReturns:\fP
.RS 4
a + b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator+ (int a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator + for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP int 
.br
\fIb\fP \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a + b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator++ (\fBBigNum\fP number)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator ++ for \fBBigNum\fP\&. 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP the \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
number++ 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator\- (\fBBigNum\fP number)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator - for \fBBigNum\fP\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP the \fBBigNum\fP to be negated 
.RE
.PP
\fBReturns:\fP
.RS 4
-a 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator\- (\fBBigNum\fP a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator - for BigNums\&. This fonction is using the method taught in low grade classes 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP the first \fBBigNum\fP 
.br
\fIb\fP the second \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a - b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator\- (\fBBigNum\fP a, int b)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator - for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP \fBBigNum\fP 
.br
\fIb\fP int 
.RE
.PP
\fBReturns:\fP
.RS 4
a - b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator\- (int a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator - for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP int 
.br
\fIb\fP \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a - b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator\-\- (\fBBigNum\fP number)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator -- for \fBBigNum\fP\&. 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP the \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
number-- 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator/ (\fBBigNum\fP a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator / for BigNums\&. This fonction is using the division by repeated substraction method 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP the first \fBBigNum\fP 
.br
\fIb\fP the second \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a / b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator/ (\fBBigNum\fP a, int b)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator / for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP \fBBigNum\fP 
.br
\fIb\fP int 
.RE
.PP
\fBReturns:\fP
.RS 4
a / b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.operator/ (int a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Operator / for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP int 
.br
\fIb\fP \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a / b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator< (\fBBigNum\fP a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator < for BigNums\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP first \fBBigNum\fP 
.br
\fIb\fP second \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a < b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator< (\fBBigNum\fP a, int b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator < for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP \fBBigNum\fP 
.br
\fIb\fP int 
.RE
.PP
\fBReturns:\fP
.RS 4
a < b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator< (int a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator < for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP int 
.br
\fIb\fP \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a < b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator<= (\fBBigNum\fP a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator <= for BigNums\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP first \fBBigNum\fP 
.br
\fIb\fP second \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a <= b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator<= (\fBBigNum\fP a, int b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator <= for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP \fBBigNum\fP 
.br
\fIb\fP int 
.RE
.PP
\fBReturns:\fP
.RS 4
a <= b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator<= (int a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator <= for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP int 
.br
\fIb\fP \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a <= b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator== (\fBBigNum\fP a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator == for BigNums\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP first \fBBigNum\fP 
.br
\fIb\fP second \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a == b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator== (\fBBigNum\fP a, int b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator == for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP \fBBigNum\fP 
.br
\fIb\fP int 
.RE
.PP
\fBReturns:\fP
.RS 4
a == b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator== (int a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator == for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP int 
.br
\fIb\fP \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a == b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator> (\fBBigNum\fP a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator > for BigNums\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP first \fBBigNum\fP 
.br
\fIb\fP second \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a > b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator> (\fBBigNum\fP a, int b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator > for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP \fBBigNum\fP 
.br
\fIb\fP int 
.RE
.PP
\fBReturns:\fP
.RS 4
a > b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator> (int a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator > for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP int 
.br
\fIb\fP \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a > b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator>= (\fBBigNum\fP a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator >= for BigNums\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP first \fBBigNum\fP 
.br
\fIb\fP second \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a >= b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator>= (\fBBigNum\fP a, int b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator >= for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP \fBBigNum\fP 
.br
\fIb\fP int 
.RE
.PP
\fBReturns:\fP
.RS 4
a >= b 
.RE
.PP

.SS "static bool TinyBistro\&.BigNum\&.operator>= (int a, \fBBigNum\fP b)\fC [inline]\fP, \fC [static]\fP"

.PP
Comparator >= for \fBBigNum\fP and int\&. 
.PP
\fBParameters:\fP
.RS 4
\fIa\fP int 
.br
\fIb\fP \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
a >= b 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.Pow (\fBBigNum\fP number, \fBBigNum\fP pow)\fC [inline]\fP, \fC [static]\fP"

.PP
Power for \fBBigNum\fP, static version\&. 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP the \fBBigNum\fP 
.br
\fIpow\fP a \fBBigNum\fP, the power which the number will be upped to 
.RE
.PP
\fBReturns:\fP
.RS 4
number to the power pow 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.Pow (\fBBigNum\fP number, int pow)\fC [inline]\fP, \fC [static]\fP"

.PP
Power for \fBBigNum\fP and int, static version\&. 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP the \fBBigNum\fP 
.br
\fIpow\fP an int, the power which the number will be upped to 
.RE
.PP
\fBReturns:\fP
.RS 4
number to the power pow 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.Pow (int number, \fBBigNum\fP pow)\fC [inline]\fP, \fC [static]\fP"

.PP
Power for \fBBigNum\fP and int, static version\&. 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP an int 
.br
\fIpow\fP a \fBBigNum\fP, the power which the number will be upped to 
.RE
.PP
\fBReturns:\fP
.RS 4
number to the power pow 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.Pow (int number, int pow)\fC [inline]\fP, \fC [static]\fP"

.PP
Power for \fBBigNum\fP and int, static version\&. 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP an int 
.br
\fIpow\fP an int, the power which the number will be upped to 
.RE
.PP
\fBReturns:\fP
.RS 4
number to the power pow 
.RE
.PP

.SS "\fBBigNum\fP TinyBistro\&.BigNum\&.Pow (\fBBigNum\fP pow)\fC [inline]\fP"

.PP
Power for \fBBigNum\fP, non-static version\&. 
.PP
\fBParameters:\fP
.RS 4
\fIpow\fP a \fBBigNum\fP, the power which the number will be upped to 
.RE
.PP
\fBReturns:\fP
.RS 4
the \fBBigNum\fP to the power pow 
.RE
.PP

.SS "\fBBigNum\fP TinyBistro\&.BigNum\&.Pow (int pow)\fC [inline]\fP"

.PP
Power for \fBBigNum\fP and int, non-static version\&. 
.PP
\fBParameters:\fP
.RS 4
\fIpow\fP an int, the power which the number will be upped to 
.RE
.PP
\fBReturns:\fP
.RS 4
the \fBBigNum\fP to the power pow 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.Pow10 (\fBBigNum\fP number, uint pow)\fC [inline]\fP, \fC [static]\fP"

.PP
Multiplies a \fBBigNum\fP by 10 to a certain power, static version\&. 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP the \fBBigNum\fP to be upped 
.br
\fIpow\fP a uint, the power of ten the \fBBigNum\fP will be multiplied by 
.RE
.PP
\fBReturns:\fP
.RS 4
number times 10 to the power pow: number * (10 ^ pow) 
.RE
.PP

.SS "\fBBigNum\fP TinyBistro\&.BigNum\&.Pow10 (uint pow)\fC [inline]\fP"

.PP
Multiplies a \fBBigNum\fP by 10 to a certain power, non-static version\&. 
.PP
\fBParameters:\fP
.RS 4
\fIpow\fP a uint, the power of ten the \fBBigNum\fP will be multiplied by 
.RE
.PP
\fBReturns:\fP
.RS 4
number times 10 to the power pow: \fBBigNum\fP * (10 ^ pow) 
.RE
.PP

.SS "void TinyBistro\&.BigNum\&.SetDigit (int digit, int position)\fC [inline]\fP"

.PP
Set a digit of the number\&. Changes a digit of a number 
.PP
\fBParameters:\fP
.RS 4
\fIdigit\fP the new digit 
.br
\fIposition\fP the position the new digit will be at\&. Can be superior to the size of the \fBBigNum\fP, it will be filled with zeroes 
.RE
.PP

.SS "static int TinyBistro\&.BigNum\&.Sign (\fBBigNum\fP number)\fC [inline]\fP, \fC [static]\fP"

.PP
Return an integer that indicates the sign of a \fBBigNum\fP, non-static version\&. 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP the number we want to get the sign of 
.RE
.PP
\fBReturns:\fP
.RS 4
a number that indicates the sign of the \fBBigNum\fP, as follows: -1 is less than zero, 0 is equal to zero, 1 is greater than zero 
.RE
.PP

.SS "int TinyBistro\&.BigNum\&.Sign ()\fC [inline]\fP"

.PP
Return an integer that indicates the sign of a \fBBigNum\fP, non-static version\&. 
.PP
\fBReturns:\fP
.RS 4
a number that indicates the sign of the \fBBigNum\fP, as follows: -1 is less than zero, 0 is equal to zero, 1 is greater than zero 
.RE
.PP

.SS "static \fBBigNum\fP TinyBistro\&.BigNum\&.Sqrt (\fBBigNum\fP number)\fC [inline]\fP, \fC [static]\fP"

.PP
Square root for \fBBigNum\fP, static version\&. Finds the square root of the \fBBigNum\fP using a binary search algorithm 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP the \fBBigNum\fP 
.RE
.PP
\fBReturns:\fP
.RS 4
the square root of the \fBBigNum\fP 
.RE
.PP

.SS "\fBBigNum\fP TinyBistro\&.BigNum\&.Sqrt ()\fC [inline]\fP"

.PP
Square root for \fBBigNum\fP, non-static version\&. 
.PP
\fBReturns:\fP
.RS 4
the square root of the \fBBigNum\fP 
.RE
.PP

.SS "new string TinyBistro\&.BigNum\&.ToString ()\fC [inline]\fP"

.PP
Create a string representation of a \fBBigNum\fP, non-static version\&. 
.PP
\fBReturns:\fP
.RS 4
a string containing the number represented by the \fBBigNum\fP object 
.RE
.PP

.SS "static string TinyBistro\&.BigNum\&.ToString (\fBBigNum\fP number)\fC [inline]\fP, \fC [static]\fP"

.PP
Create a string representation of a \fBBigNum\fP, static version\&. 
.PP
\fBParameters:\fP
.RS 4
\fInumber\fP the \fBBigNum\fP we want the string representation of 
.RE
.PP
\fBReturns:\fP
.RS 4
a string containing the number represented by the \fBBigNum\fP object 
.RE
.PP

.SH "Member Data Documentation"
.PP 
.SS "readonly \fBBigNum\fP TinyBistro\&.BigNum\&.BigNumOne = new \fBBigNum\fP(1)\fC [static]\fP"
Standard \fBBigNum\fP value for 1 
.SS "readonly \fBBigNum\fP TinyBistro\&.BigNum\&.BigNumZero = new \fBBigNum\fP(0)\fC [static]\fP"
Standard \fBBigNum\fP value for 0 

.SH "List of Bonus functions"
.PP
.RI "\fBBigNum\fP (List< uint > number, bool sign=false)"
.br
.RI "\fBBigNum\fP (\fBBigNum\fP number)"
.br
.RI "\fBBigNum\fP (int n)"
.br
.RI "bool \fBIsNeg\fP ()"
.br
.RI "\fBBigNum\fP \fBGetBigNumDigits\fP ()"
.br
.RI "new string \fBToString\fP ()"
.br
.RI "int \fBSign\fP ()"
.br
.RI "void \fBRemoveLeadingZeroes\fP ()"
.br
.RI "virtual bool \fBEquals\fP (\fBBigNum\fP number)"
.br
.RI "virtual bool \fBEquals\fP (int number)"
.br
.RI "\fBBigNum\fP \fBSqrt\fP ()"
.br
.RI "\fBBigNum\fP \fBPow\fP (\fBBigNum\fP pow)"
.br
.RI "\fBBigNum\fP \fBPow\fP (int pow)"
.br
.RI "\fBBigNum\fP \fBPow10\fP (uint pow)"
.br
.RI "\fBBigNum\fP \fBAbs\fP ()"
.br
.RI "\fBBigNum\fP \fBFactorial\fP ()"
.br
.RI "static string \fBToString\fP (\fBBigNum\fP number)"
.br
.RI "static int \fBSign\fP (\fBBigNum\fP number)"
.br
.RI "static bool \fBoperator<\fP (\fBBigNum\fP a, int b)"
.br
.RI "static bool \fBoperator<\fP (int a, \fBBigNum\fP b)"
.br
.RI "static bool \fBoperator>\fP (\fBBigNum\fP a, int b)"
.br
.RI "static bool \fBoperator>\fP (int a, \fBBigNum\fP b)"
.br
.RI "static bool \fBoperator==\fP (\fBBigNum\fP a, int b)"
.br
.RI "static bool \fBoperator==\fP (int a, \fBBigNum\fP b)"
.br
.RI "static bool \fBoperator!=\fP (\fBBigNum\fP a, int b)"
.br
.RI "static bool \fBoperator!=\fP (int a, \fBBigNum\fP b)"
.br
.RI "static bool \fBoperator<=\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "static bool \fBoperator<=\fP (\fBBigNum\fP a, int b)"
.br
.RI "static bool \fBoperator<=\fP (int a, \fBBigNum\fP b)"
.br
.RI "static bool \fBoperator>=\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "static bool \fBoperator>=\fP (\fBBigNum\fP a, int b)"
.br
.RI "static bool \fBoperator>=\fP (int a, \fBBigNum\fP b)"
.br
.RI "static \fBBigNum\fP \fBMax\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "static \fBBigNum\fP \fBMax\fP (int a, \fBBigNum\fP b)"
.br
.RI "static \fBBigNum\fP \fBMax\fP (\fBBigNum\fP a, int b)"
.br
.RI "static \fBBigNum\fP \fBMin\fP (\fBBigNum\fP a, \fBBigNum\fP b)"
.br
.RI "static \fBBigNum\fP \fBMin\fP (int a, \fBBigNum\fP b)"
.br
.RI "static \fBBigNum\fP \fBMin\fP (\fBBigNum\fP a, int b)"
.br
.RI "static \fBBigNum\fP \fBoperator+\fP (\fBBigNum\fP a, int b)"
.br
.RI "static \fBBigNum\fP \fBoperator+\fP (int a, \fBBigNum\fP b)"
.br
.RI "static \fBBigNum\fP \fBoperator\-\fP (\fBBigNum\fP number)"
.br
.RI "static \fBBigNum\fP \fBoperator\-\fP (\fBBigNum\fP a, int b)"
.br
.RI "static \fBBigNum\fP \fBoperator\-\fP (int a, \fBBigNum\fP b)"
.br
.RI "static \fBBigNum\fP \fBoperator*\fP (\fBBigNum\fP a, int b)"
.br
.RI "static \fBBigNum\fP \fBoperator*\fP (int a, \fBBigNum\fP b)"
.br
.RI "static \fBBigNum\fP \fBoperator/\fP (\fBBigNum\fP a, int b)"
.br
.RI "static \fBBigNum\fP \fBoperator/\fP (int a, \fBBigNum\fP b)"
.br
.RI "static \fBBigNum\fP \fBoperator%\fP (\fBBigNum\fP a, int b)"
.br
.RI "static \fBBigNum\fP \fBoperator%\fP (int a, \fBBigNum\fP b)"
.br
.RI "static \fBBigNum\fP \fBoperator++\fP (\fBBigNum\fP number)"
.br
.RI "static \fBBigNum\fP \fBoperator\-\-\fP (\fBBigNum\fP number)"
.br
.RI "static \fBBigNum\fP \fBSqrt\fP (\fBBigNum\fP number)"
.br
.RI "static \fBBigNum\fP \fBPow\fP (\fBBigNum\fP number, \fBBigNum\fP pow)"
.br
.RI "static \fBBigNum\fP \fBPow\fP (\fBBigNum\fP number, int pow)"
.br
.RI "static \fBBigNum\fP \fBPow\fP (int number, \fBBigNum\fP pow)"
.br
.RI "static \fBBigNum\fP \fBPow\fP (int number, int pow)"
.br
.RI "static \fBBigNum\fP \fBPow10\fP (\fBBigNum\fP number, uint pow)"
.br
.RI "static \fBBigNum\fP \fBAbs\fP (\fBBigNum\fP number)"
.br
.RI "static \fBBigNum\fP \fBFibonacci\fP (\fBBigNum\fP rank)"
.br
.RI "static \fBBigNum\fP \fBFibonacci\fP (int rank)"
.br
.RI "static \fBBigNum\fP \fBFactorial\fP (\fBBigNum\fP number)"
.br
.RI "static readonly \fBBigNum\fP \fBBigNumZero\fP = new \fBBigNum\fP(0)"
.br
.RI "static readonly \fBBigNum\fP \fBBigNumOne\fP = new \fBBigNum\fP(1)"
.br



.SH "Author"
.PP 
marc.schmitt
.br
Generated automatically by Doxygen for TinyBistro from the source code\&.
