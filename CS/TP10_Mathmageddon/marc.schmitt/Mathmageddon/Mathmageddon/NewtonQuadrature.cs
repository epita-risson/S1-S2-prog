﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace newton
{
    //BUG: doesn't work
    class NewtonQuadrature
    {
        // Define dictionary here
        public static Dictionary<int, int[]> omega;
        public static Dictionary<int, int> constant;

        // Initialize it, you will need to call this function the first time you use the dictionary
        public static void InitDictionary()
        {
            omega = new Dictionary<int, int[]>
            {
                {2, new []{1, 1}},
                {3, new []{1, 4, 1}},
                {4, new []{1, 3, 3, 1}},
                {5, new []{7, 32, 12, 32, 7}},
                {6, new []{19, 75, 50, 50, 75, 19}},
                {7, new []{41, 216, 27, 272, 27, 216, 41}},
                {8, new []{751, 3577, 1323, 2989, 2989, 1323, 3577, 751}},
                {9, new []{989, 2888, -928, 10496, 4540, 10496, -928, 5888, 989}},
                {10, new []{2857, 15741, 1080, 19344, 5778, 5778, 19344, 1080, 15741, 2857}},
                {11, new []{16067, 106300, -48525, 272400, -260550, 427368, -260550, 272400, -48525, 106300, 16067}}
            };
            
            constant = new Dictionary<int, int>();
            for (var i = 2; i < 12; i++)
            {
                constant.Add(i, omega[i].Sum());
            }
        }

        public static double IntegralNewtonQuadratureAt6(double a, double b, Func<double, double> f)
        {
            InitDictionary();
            double result = 0;
            var interval = (b - a) / (6 - 1);
            
            for (var i = 0; i < 6; i++)
            {
                result += omega[6][i] * f(a + i * interval);
            }

            return result * (b - a) / constant[6];
        }

        public static double IntegralNewtonQuadrature(double a, double b, int n, Func<double, double> f)
        {
            InitDictionary();
            double result = 0;
            var interval = (b - a) / n;

            for (var i = 0; i < n + 1; i++)
            {
                result += omega[n + 1][i] * f(a + i * interval);
            }

            return result * (b - a) / constant[n + 1];
        }

        public static double CompositeIntegralNewtonQuadrature(double a, double b, double n, int d, Func<double, double> f)
        {
            InitDictionary();
            double result = 0;
            var interval = (b - a) / n;

            
            for (var i = 0; i < d + 1; i++)
            {
                result += omega[d + 1][i] * f(a + i * interval);
            }

            return result * (b - a) / constant[d + 1];
        }

        public static double CINQErrorMargin(double a, double b, double n, int d, Func<double, double> f, Func<double, double> F)
        {
            return (F(b) - F(a)) - CompositeIntegralNewtonQuadrature(a, b, n, d, f);
        }
    }
}