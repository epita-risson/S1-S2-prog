﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace derivative
{
    class Derivative
    {
        public static double RateOfChange(double a, double h, Func<double, double> f)
        {
            if (h == 0)
            {
                throw new ArgumentException("h can't be equal to 0", nameof(h));
            }
            return (f(a + h) - f(a)) / h;
        }

        public static List<double> GeneratePointsForPlot(double a, double b, double t, Func<double, double> f)
        {
            var result = new List<double>();
            for (; a < b; a += t)
            {
                result.Add(RateOfChange(a, b - a, f));
            }
            if (a != b)
            {
                result.Add(RateOfChange(b, b - a, f));
            }
            return result;
        }
    }
}