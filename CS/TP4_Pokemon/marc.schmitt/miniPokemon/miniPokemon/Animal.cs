﻿using System;

namespace miniPokemon
{
    public class Animal
    {
        private string name;

        public string Name
        {
            get => name;
            private set => name = value;
        }

        #region Constructor

        public Animal(string name)
        {
            Name = name;
        }

        #endregion Constructor

        #region Methods

        public virtual void WhoAmI()
        {
            Console.WriteLine("I am an animal !");
        }

        public virtual void Describe()
        {
            Console.WriteLine("My name is {0}.", Name);
        }

        public void Rename(string newName)
        {
            Name = newName;
        }

        #endregion Methods
    }
}