﻿using System;
using System.Drawing;

namespace TinyPhotoshop
{
    public static class Geometry
    {      
        public static Image Resize(Bitmap img, int x, int y)
        {
	        //TODO: FIXME
	        var width = img.Width;
	        var height = img.Height;
	        var xRatio = x / width;
	        var yRatio = y / height;
	        var result = new Bitmap(img, x, y);
	        for (var i = 0; i < x; i++)
	        {
		        for (var j = 0; j < y; j++)
		        {
			        
		        }
	        }
	        return result;
        }
        
        public static Image Shift(Bitmap img, int x, int y)
        {
	        var width = img.Width;
	        var height = img.Height;
	        var result = new Bitmap(width, height);
	        for (var i = 0; i < width; i++)
	        {
		        for (var j = 0; j < height; j++)
		        {
			        result.SetPixel((i + x) % width, (j + y) % height, img.GetPixel(i, j));
		        }
	        }
	        return result;
        }

        public static Image SymmetryHorizontal(Bitmap img)
        {
	        var width = img.Width;
	        var height = img.Height;
	        var result = new Bitmap(width, height);
	        for (var x = 0; x < width; x++)
	        {
		        for (var y = 0; y < height; y++)
		        {
			        result.SetPixel(x, height - 1 - y, img.GetPixel(x, y));
		        }
	        }
	        return result;
		}
        
        public static Image SymmetryVertical(Bitmap img)
        {
	        var width = img.Width;
	        var height = img.Height;
	        var result = new Bitmap(width, height);
	        for (var x = 0; x < width; x++)
	        {
		        for (var y = 0; y < height; y++)
		        {
			        result.SetPixel(width - 1 - x, y, img.GetPixel(x, y));
		        }
	        }
	        return result;
		}
        
        public static Image SymmetryPoint(Bitmap img, int x, int y)
        {
	        //TODO: just do it
	        var width = img.Width;
	        var height = img.Height;
	        var result = new Bitmap(height, width);
	        /*int xp = 0, yp = 0;
	        for (var i = 0; i < width; i++)
	        {
		        for (var j = 0; j < height; j++)
		        {
			        if (i - x <= 0)
			        {
				        xp = (x + x - i) >= img.Width ? (x + x - i) - img.Width : (x + x - i);
			        }
			        else
			        {
				        xp = (x - (i - x)) < 0 ? img.Width + (x - (i - x)) : (x - (i - x));
			        }
			        if (j - y <= 0)
			        {
				        yp = y + y - j >= img.Height ? y + y - j - img.Width : y + y - j;
			        }
			        else
			        {
				        yp = y - (j - y) < 0 ? img.Height + y - (j - y) : y - (j - y);
			        }
			        result.SetPixel(i, j, img.GetPixel(xp, yp));		        }
	        }*/
	        return result;
		}
        
        public static Image RotationLeft(Bitmap img)
        {
	        var width = img.Width;
	        var height = img.Height;
	        var result = new Bitmap(height, width);
	        for (var x = 0; x < width; x++)
	        {
		        for (var y = 0; y < height; y++)
		        {
			        result.SetPixel(y, width - 1 - x, img.GetPixel(x, y));
		        }
	        }
	        return result;
        }
        
        public static Image RotationRight(Bitmap img)
        {
	        //return RotationLeft((Bitmap) RotationLeft((Bitmap) RotationLeft(img)));
	        var width = img.Width;
	        var height = img.Height;
	        var result = new Bitmap(height, width);
	        for (var x = 0; x < width; x++)
	        {
		        for (var y = 0; y < height; y++)
		        {
			        result.SetPixel(height - 1 - y, x, img.GetPixel(x, y));
		        }
	        }
	        return result;
        }
    }
}