﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace EvalExpr
{
    public static class Lexer
    {
        private static bool IsDigit(char c)
        {
            return c > 47 && c < 58;
        }

        public static Token Tokenize(ref int pos, string expr)
        {
            if (IsDigit(expr[pos]))
            {
                var n = "";
                while (pos < expr.Length && IsDigit(expr[pos]))
                {
                    n += expr[pos];
                    pos++;
                }
                return new Token(Token.Type.INT, n.ToString());
            }
            if (expr[pos] == '+')
            {
                pos++;
                return new Token(Token.Type.PLUS, "+");
            }
            if (expr[pos] == '-')
            {
                pos++;
                return new Token(Token.Type.MINUS, "-");
            }
            if (expr[pos] == '*')
            {
                pos++;
                return new Token(Token.Type.MULT, "*");
            }
            if (expr[pos] == '/')
            {
                pos++;
                return new Token(Token.Type.DIV, "/");
            }
            throw new ArgumentException("Lexer.Tokenize: Not a valid character: " + expr[pos], nameof(expr));
        }
        
        public static List<Token> Lex(string expr)
        {
            expr = EatBlank(expr);
            var result = new List<Token>();
            var pos = 0;
            var n = expr.Length;
            while (pos < n) result.Add(Tokenize(ref pos, expr));
            return result;
        }
        
        private static string EatBlank(string expr)
        {
            var result = "";
            foreach (var c in expr)
            {
                if (c != ' ' && c != '\n' && c != 11)
                {
                    result += c;
                }
            }
            return result;
        }
    }
}