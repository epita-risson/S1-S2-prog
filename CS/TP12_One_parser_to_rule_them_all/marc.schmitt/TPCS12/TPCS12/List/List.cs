﻿using System;
using System.IO;

namespace List
{
    public class List<T>
    {
        protected Node<T> head_;
        protected Node<T> tail_;

        public List()
        {
            head_ = null;
            tail_ = null;
        }

        public List(T data)
        {
            head_ = new Node<T>(data);
            tail_ = new Node<T>(data);
        }

        public void print()
        {
            var count = Count;
            if (count == 0) 
            {
                Console.WriteLine();
                return;
            }
            for (var i = 0; i < count - 1; i++)
            {
                Console.Write(this[i] + ", ");
            }
            Console.WriteLine(this[count - 1]);
        }
 
        public T this[int i]
        {
            get => GetNthElement(i).Data;
            set => GetNthElement(i).Data = value;
        }
        
        //TODO: README
        private Node<T> GetNthElement(int index)
        {
            if (index < 0 || index >= Count) throw new IndexOutOfRangeException();
            if (Count == 1) return head_;
            
            var elt = head_;
            for (var i = 0; i <= index; i++)
            {
                elt = elt.Next;
            }

            return elt;
        }

        //TODO: README
        public int Count
        {
            get
            {
                if (head_ == null || tail_ == null) return 0;
                
                var count = 0;
                var elt = head_;
                while (elt != null)
                {
                    elt = elt.Next;
                    count++;
                }
                return count;
            }
        }

        public void insert(int i, T value)
        {
            var count = Count;
            if (i < 0 || i > count) throw new IndexOutOfRangeException();

            if (count == 0)
            {
                head_ = new Node<T>(value);
                tail_ = new Node<T>(value);
            }
            else if (i == 0)
            {
                var oldHead = head_.Data;
                head_.Data = value;
                if (count == 1)
                {
                    head_.Next = tail_;
                    tail_.Prev = head_;
                    head_.Next = tail_;
                }
                else
                {
                    insert(1, oldHead);
                }
            }
            else
            {
                var elt = new Node<T>(value);
                var previous = GetNthElement(i - 1);
                var next = previous.Next;

                previous.Next = elt;
                elt.Prev = previous;

                next.Prev = elt;
                elt.Next = next;
            }
        }

        //TODO: README
        public void append(T value)
        {
            insert(Count, value);
        }

        public void delete(int i)
        {
            var count = Count;
            
            if (i < 0 || i >= count) throw new IndexOutOfRangeException();
            
            if (count == 1)
            {
                head_ = null;
                tail_ = null;
            }

            else if (i == 0)
            {
                head_ = head_.Next;
                head_.Prev = null;
            }

            else if (i == count - 1)
            {
                tail_ = tail_.Prev;
                tail_.Next = null;
            }

            else
            {
                var previous = GetNthElement(i - 1);
                var next = previous.Next.Next;
                previous.Next = next;
                next.Prev = previous;
            }
        }
    }
}