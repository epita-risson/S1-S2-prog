﻿using System;
using System.IO;
using System.Linq.Expressions;

namespace Exercise1
{
	internal static class PrintFile
	{
		/// <summary>
		///   EXERCISE 1.1:
		///   <para />
		///   Read all the text in a file and print it in the console.
		/// </summary>
		/// <param name="path">the path (absolute or relative) to the file</param>
		public static void PrintAllFile(string path)
		{
			try
			{
				var lines = File.ReadAllLines(path);
				foreach(var line in lines)
				{
					Console.WriteLine(line);
				}
			}
			catch (DirectoryNotFoundException)
			{
				Console.WriteLine("could not open file: {0}", path);
			}
		}

		/// <summary>
		///   EXERCISE 1.2:
		///   <para />
		///   Read only one line out of two and print it in the console.
		/// </summary>
		/// <param name="path"></param>
		public static void PrintHalfFile(string path)
		{
			try
			{
				var lines = File.ReadAllLines(path);
				for (var i = 0; i < lines.Length; i += 2)
				{
					Console.WriteLine(lines[i]);
				}
			}
			catch (DirectoryNotFoundException)
			{
				Console.WriteLine("could not open file: {0}", path);
			}

		}
	}
}