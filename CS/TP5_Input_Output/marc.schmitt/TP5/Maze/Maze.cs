﻿using System;
using System.IO;

namespace Maze
{
    internal static class Maze
    {		
        /// <summary>
        /// MAZE:
        /// <para/>
        /// This is where you should call your functions to make your program work.
        /// </summary>
        /// <param name="args">unused</param>
        public static void Main(string[] args)
        {
            // get .maze filename
            var path = AskMazeFile();
            //var path = "../../../tests/map4.maze";
			
            // parse .maze file
            var grid = ParseFile(path);
			
            // print the maze (BONUS)
            PrintGrid(grid);
			
            // solve the maze
            SolveMaze(grid);
			
            // print the maze (BONUS)
            PrintGrid(grid);
			
            // get .solved filename
            // save solution in .solved file
            SaveSolution(grid, GetOutputFile(path));
			
            // say goo'bye !
            Console.WriteLine("> Thank you, bye");
        }

        // Asks the file where the .maze file is located to the user
        private static string AskMazeFile()
        {
            var path = "";
            while (path == "" || !File.Exists(path) || Path.GetExtension(path) != ".maze")
            {
                Console.WriteLine("> Which file should be loaded?");
                path = Console.ReadLine();
            }
            return path;
        }

        // Returns the path to the .solve file to be created
        private static string GetOutputFile(string fileIn)
        {
            return Path.ChangeExtension(fileIn, ".solved");
        }

        // Converts a single-line string into a char[]
        private static char[] ParseLine(string line)
        {
            var n = line.Length;
            var result = new char[n];
            for (var i = 0; i < n; i++)
            {
                result[i] = line[i];
            }
            return result;
        }

        // Converts a multi-line string into a char[][]
        private static char[][] ParseFile(string path)
        {
            var lines = File.ReadAllLines(path);
            var n = lines.Length;
            var result = new char[n][];
            for (var i = 0; i < n; i++)
            {
                result[i] = ParseLine(lines[i]);
            }
            return result;
        }

        // Finds the start point of the maze
        public static Point FindStart(char[][] grid)
        {
            for (var i = 0; i < grid.GetLength(0); i++)
            {
                for (var j = 0; j < grid[i].Length; j++)
                {
                    if (grid[i][j] == 'S')
                    {
                        return new Point(i, j);
                    }
                }
            }
            throw new Exception("FindStart: Grid has no START square");
        }

        // Finds the finish point of the maze
        public static Point FindFinish(char[][] grid)
        {
            for (var i = 0; i < grid.GetLength(0); i++)
            {
                for (var j = 0; j < grid[i].Length; j++)
                {
                    if (grid[i][j] == 'F')
                    {
                        return new Point(i, j);
                    }
                }
            }
            throw new Exception("FindStart: Grid has no FINISH square");
        }

        // Asks the user which method to use to solve the maze, and then solves it
        private static void SolveMaze(char[][] grid)
        {
            var input = "";
            while (input != "1" && input != "2")
            {
                Console.WriteLine("Which resolution method do you want to use?");
                Console.WriteLine("1. Back tracking method (faster, but not the best path)");
                Console.WriteLine("2. Weight recording (recommended, similar to A*, a bit slower, best path)");
                input = Console.ReadLine();
                
            }
            Console.Clear();
            PrintGrid(grid);
            var method = int.Parse(input);
            if (method == 1)
            {
                var n = grid.GetLength(0);
                var processed = new int[n][];
                for (var i = 0; i < n; i++)
                {
                    processed[i] = new int[grid[i].Length];
                }
                foreach (var row in processed)
                {
                    for (var c = 0; c < row.Length; c++) // C what i did there?
                    {
                        row[c] = 0;
                    }
                }
                var start = FindStart(grid);
                if (!SolveMazeBackTracking(grid, processed, start))
                {
                    throw new Exception("Could not solve grid");
                }
                grid[start.X][start.Y] = 'S';
            }
            else if (method == 2)
            {
                if (!MazeSolveWeightRecording.SolveMaze(grid))
                {
                    throw new Exception("Could not solve grid");
                }
            }
            else
            {
                throw new Exception("Give up. Just give up.");
            }
        }

        // Solves the maze using a back tracking method
        private static bool SolveMazeBackTracking(char[][] grid, int[][] processed, Point p)
        {
            if (!p.IsInGrid(grid))
            {
                return false;
            }
			
            if (processed[p.X][p.Y] == 1)
            {
                return false;
            }
			
            processed[p.X][p.Y] = 1;
            switch (grid[p.X][p.Y])
            {
                case 'F':
                    return true;
                case 'B':
                    return false;
                default:
                    break;
            }
            if (SolveMazeBackTracking(grid, processed, new Point(p.X - 1, p.Y)))
            {
                grid[p.X][p.Y] = 'P';
                return true;
            }
            if (SolveMazeBackTracking(grid, processed, new Point(p.X, p.Y - 1)))
            {
                grid[p.X][p.Y] = 'P';
                return true;
            }
            if (SolveMazeBackTracking(grid, processed, new Point(p.X, p.Y + 1)))
            {
                grid[p.X][p.Y] = 'P';
                return true;
            }
            if (SolveMazeBackTracking(grid, processed, new Point(p.X + 1, p.Y)))
            {
                grid[p.X][p.Y] = 'P';
                return true;
            }
            return false;
        }

        // Converts a char[][] into a multi-line string
        private static string ParseGrid(char[][] grid)
        {
            var result = "";
            foreach (var row in grid)
            {
                result = result + string.Join("", row) + "\n";
            }
            return result;		
        }
		
        // Saves the solution to the maze into a file
        private static void SaveSolution(char[][] grid, string fileOut)
        {
            File.WriteAllText(fileOut, ParseGrid(grid));
        }

        // Prints the maze
        public static void PrintGrid(char[][] grid)
        {
            var result = ParseGrid(grid);
            foreach (var line in result)
            {
                switch (line)
                {
                    case 'B':
                        Console.BackgroundColor = ConsoleColor.DarkGray;
                        Console.Write(' ');
                        break;
                    case 'P':
                        Console.BackgroundColor = ConsoleColor.Yellow;
                        Console.Write(' ');
                        break;
                    case 'O':
                        Console.BackgroundColor = ConsoleColor.Blue;
                        Console.Write(' ');
                        break;
                    case 'S':
                        Console.BackgroundColor = ConsoleColor.DarkRed;
                        Console.Write(' ');
                        break;
                    case 'F':
                        Console.BackgroundColor = ConsoleColor.DarkGreen;
                        Console.Write(' ');
                        break;
                    case '\n':
                        Console.WriteLine();
                        break;
                    default:
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.Write(' ');
                        break;
                }
                Console.ResetColor();
            }
            Console.ResetColor();
            Console.WriteLine();
        }
    }

    /// <summary>
    ///   Class that allows to store 2D coordinates.
    /// </summary>
    internal class Point
    {
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
        public int X { get; set; }
        public int Y { get; set; }

        // Checks if a point's coordinates are in a grid
        public bool IsInGrid(int[][] grid)
        {
            return !(X < 0 || X >= grid.GetLength(0) || Y < 0 || Y >= grid[0].Length);
        }
        public bool IsInGrid(char[][] grid)
        {
            return !(X < 0 || X >= grid.GetLength(0) || Y < 0 || Y >= grid[0].Length);
        }
    }
	
    /// <summary>
    ///   Class that allows to solve the maze using a A*-like method
    /// </summary>
    internal static class MazeSolveWeightRecording
    {
        // Main function
        public static bool SolveMaze(char[][] grid)
        {
            var start = Maze.FindStart(grid);
            var finish = Maze.FindFinish(grid);
            var n = grid.GetLength(0);
            var weights = new int[n][];
            for (var i = 0; i < n; i++)
            {
                weights[i] = new int[grid[i].Length];
            }
            foreach (var row in weights)
            {
                for (var c = 0; c < row.Length; c++) // C what i did there?
                {
                    row[c] = -1;
                }
            }
            if (!SolveMazeWeight(grid, weights, start, 1))
            {
                return false;
            }
            ReconstitutePath(grid, weights, start, finish); 
            return true;
        }
		
        // Finds all the possible paths
        private static bool SolveMazeWeight(char[][] grid, int[][] weights, Point p, int weight)
        {
            if (!p.IsInGrid(weights))
            {
                return false;
            }
            if (weights[p.X][p.Y] != -1 && weights[p.X][p.Y] < weight)
            {
                return false;
            }
            switch (grid[p.X][p.Y])
            {
                case 'F':
                    weights[p.X][p.Y] = weight;
                    return true;
                case 'B':
                    return false;
                default:
                    break;
            }
            weights[p.X][p.Y] = weight;
            return SolveMazeWeight(grid, weights, new Point(p.X - 1, p.Y), weight + 1) |
                   SolveMazeWeight(grid, weights, new Point(p.X, p.Y - 1), weight + 1) |
                   SolveMazeWeight(grid, weights, new Point(p.X, p.Y + 1), weight + 1) |
                   SolveMazeWeight(grid, weights, new Point(p.X + 1, p.Y), weight + 1);
        }

        // Compares the weight of to points, and returns the lighter one
        private static Point MinimumPoint(Point p1, Point p2, int[][] weights)
        {
            if (!p1.IsInGrid(weights))
            {
                return p2;
            }
            if (!p2.IsInGrid(weights))
            {
                return p1;
            }
            if (weights[p1.X][p1.Y] == -1)
            {
                return p2;
            }
            if (weights[p2.X][p2.Y] == -1)
            {
                return p1;
            }
            return weights[p1.X][p1.Y] < weights[p2.X][p2.Y] ? p1 : p2;
        }

        // Finds the best path
        private static void ReconstitutePath(char[][] grid, int[][] weights, Point start, Point finish)
        {
            var x = finish.X;
            var y = finish.Y;
            while (x != start.X || y != start.Y)
            {
                var up = new Point(x + 1, y);
                var down = new Point(x - 1, y);
                var left = new Point(x, y + 1);
                var right = new Point(x, y - 1);
                var pMin = MinimumPoint(MinimumPoint(up, down, weights), MinimumPoint(left, right, weights), weights);
                if (!pMin.IsInGrid(weights))
                {
                    throw new Exception("give up. just give up.");
                }
                grid[pMin.X][pMin.Y] = 'P';
                x = pMin.X;
                y = pMin.Y;
            }
            grid[start.X][start.Y] = 'S';
        }
    }
}