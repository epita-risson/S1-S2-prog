﻿namespace EpitaSpaceProgram
{
    public class Infinity : CompositeBody
    {
        public Infinity(string name, double mass, double density, Vector2 initialPosition, Vector2 origin,
            double spring)
            : base(name, mass, density, initialPosition)
        {
            Add(new Spring(name, mass, density, initialPosition, origin, spring));
            Add(new SpringMax(name, mass, density, new Vector2(initialPosition.Y, initialPosition.X), origin, spring * 4));
        }
    }
}