﻿using System;
using System.Collections.Generic;
using System.Linq;
using EpitaSpaceProgram.ACDC;

namespace EpitaSpaceProgram
{
    public class System : IEntity
    {
        // Use this list to stores your `Body` objects.
        private readonly List<Body> _bodies;
        private readonly double _g;

        public System(double g)
        {
            _bodies = new List<Body>();
            _g = g;
        }


        public void Update(double delta)
        {
            foreach (var body1 in _bodies)
            {
                foreach (var body2 in _bodies)
                {
                    if (body1 == body2)
                    {
                        continue;
                    }
                    var AB = new Vector2(body2.Position - body1.Position);
                    var F = AB.Normalized() * _g * (body1.Mass * body2.Mass) /
                            Math.Pow(Vector2.Dist(body1.Position, body2.Position), 2);
                    body1.ApplyForce(F);
                }
            }

            foreach (var body in _bodies)
            {
                body.Update(delta);
            }
        }

        // Do not edit this method.
        public IEnumerable<string> Serialize()
        {
            return _bodies.SelectMany(body => body.Serialize());
        }

        public void Add(Body body)
        {
            _bodies.Add(body);
        }
    }
}