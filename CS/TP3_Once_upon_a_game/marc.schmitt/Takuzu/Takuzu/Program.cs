﻿using System;

namespace Takuzu
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            /*
            // Using a second grid to test the AI because grid isn't solvable by my AI
            int[,] grid2 =
            {
                {-1, -1, 0, -1, 0, -1, -1, 0},
                {-1, -1, -1, -1, -1, -1, 1, -1},
                {-1, -1, -1, -1, 0, -1, -1, 0},
                {-1, 1, -1, -1, -1, -1, 0, -1},
                {-1, 1, -1, -1, -1, -1, -1, -1},
                {-1, -1, -1, 0, 0, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1, 1, -1},
                {-1, -1, 1, -1, 0, 0, -1, 0}
            };
            
            int[,] grid =
            {
                {-1, -1, 0, -1, -1, -1, -1, -1},
                {1, -1, 1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, 0, 0, -1, -1},
                {-1, -1, -1, 1, -1, -1, -1, -1},
                {-1, -1, 0, -1, -1, -1, -1, 0},
                {-1, -1, -1, -1, 0, -1, -1, 0},
                {0, -1, -1, 0, 0, -1, -1, -1},
                {0, -1, -1, -1, -1, 0, 0, -1}
            };
            */
            //Takuzu.PrintGrid(grid);
            //Takuzu.Game(grid);
            //Takuzu.PrintGrid(Takuzu.AI(grid2));
            //TakuzuSolver.AI2(grid2);
        }
    }
}