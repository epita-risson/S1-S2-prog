﻿using System;

namespace Takuzu
{
    public static class Takuzu
    {
        // Prints out a Takuzu grid
        public static void PrintGrid(int[,] grid)
        {
            Console.Write("x\\y");
            for (var i = 0; i < grid.GetLength(0); i++)
            {
                Console.Write("{0} ", i);
            }
            Console.WriteLine();
            for (var i = 0; i < grid.GetLength(0); i++)
            {
                Console.Write("{0} |", i);
                for (var j = 0; j < grid.GetLength(1); j++)
                {
                    switch (grid[i, j])
                    {
                            case 0:
                                Console.Write("0|");
                                break;
                            case 1:
                                Console.Write("1|");
                                break;
                            default:
                                Console.Write(" |");
                                break;
                    }
                }
                Console.WriteLine();
            }
        }
        
        // Inverses rows and columns of a 2D array
        public static int[,] Reverse(int[,] grid)
        {
            var rows = grid.GetLength(0);
            var columns = grid.GetLength(1);
            var result = new int[rows, columns];
            for (var i = 0; i < rows; i++)
            {
                for (var j = 0; j < columns; j++)
                {
                    result[j, i] = grid[i, j];
                }
            }
            return result;
        }

        public static void CountRow(int[,] grid, int row, out int zeroes, out int ones, out int others)
        {
            zeroes = 0;
            ones = 0;
            others = 0;
            var columns = grid.GetLength(1);
            for (var i = 0; i < columns; i++)
            {
                switch (grid[row, i])
                {
                    case 0:
                        zeroes++;
                        break;
                    case 1:
                        ones++;
                        break;
                    default:
                        others++;
                        break;
                }
            }

        }

        public static void CountColumns(int[,] grid, int col, out int zeroes, out int ones, out int others)
        {
            zeroes = 0;
            ones = 0;
            others = 0;
            var rows = grid.GetLength(0);
            for (var i = 0; i < rows; i++)
            {
                switch (grid[i, col])
                {
                    case 0:
                        zeroes++;
                        break;
                    case 1:
                        ones++;
                        break;
                    default:
                        others++;
                        break;
                }
            }
        }
        
        // Checks if a row is valid
        public static bool IsRowValid(int[,] grid, int row)
        {
            var columns = grid.GetLength(1);
            if (columns == 2)
            {
                return !(grid[row, 0] == 0 && grid[row, 1] == 0 || grid[row, 0] == 1 && grid[row, 1] == 1);
            }
            for (var i = 0; i < columns - 2; i++)
            {
                if (grid[row, i] == 0 && grid[row, i + 1] == 0 && grid[row, i + 2] == 0 || grid[row, i] == 1 && grid[row, i + 1] == 1 && grid[row, i + 2] == 1)
                {
                    return false;
                }
            }
            int zeroes, ones, others;
            CountRow(grid, row, out zeroes, out ones, out others);
            return others != 0 || zeroes == ones;
        }
        
        // Checks if a column is valid
        // Suggested amelioration (speedtest to be made first):
        // invert the grid and then check if the rows of the inverted grid are valid
        // probably slower
        public static bool IsColumnValid(int[,] grid, int col)
        {
            var rows = grid.GetLength(0);
            if (rows == 2)
            {
                return !(grid[0, col] == 0 && grid[1, col] == 0 || grid[0, col] == 1 && grid[1, col] == 1);
            }
            for (var i = 0; i < rows - 2; i++)
            {
                if (grid[i, col] == 0 && grid[i + 1, col] == 0 && grid[i + 2, col] == 0 || grid[i, col] == 1 && grid[i + 1, col] == 1 && grid[i + 2, col] == 1)
                {
                    return false;
                }
            }
            int zeroes, ones, others;
            CountColumns(grid, col, out zeroes, out ones, out others);
            return others != 0 || zeroes == ones;
        }

        // Checks if a grid is valid
        public static bool IsGridValid(int[,] grid)
        {
            var rows = grid.GetLength(0);
            var columns = grid.GetLength(1);
            // Even number of cells
            if ((rows * columns) % 2 != 0)
            {
                return false;
            }
            // Rows validity
            for (var i = 0; i < rows; i++)
            {
                if (!IsRowValid(grid, i))
                {
                    return false;
                }
            }
            // Columns validity
            for (var i = 0; i < columns; i++)
            {
                if (!IsColumnValid(grid, i))
                {
                    return false;
                }
            }
            // Rows unicity
            for (var i = 0; i < rows; i++)
            {
                for (var j = 0; j < rows; j++)
                {
                    if (i == j)
                    {
                        continue;
                    }
                    var unique = true;
                    for (var n = 0; n < columns; n++)
                    {
                        if (grid[i, n] == -1 && grid[j, n] == -1)
                        {
                            continue;
                        }
                        unique &= (grid[i, n] == grid[j, n]);
                    }
                    if (unique)
                    {
                        return false;
                    }
                }
            }
            // Columns unicity
            for (var i = 0; i < columns; i++)
            {
                for (var j = 0; j < columns; j++)
                {
                    if (i == j)
                    {
                        continue;
                    }
                    var unique = true;
                    for (var n = 0; n < rows; n++)
                    {
                        if (grid[n, i] == -1 && grid[n, j] == -1)
                        {
                            continue;
                        }
                        unique &= (grid[n, i] == grid[n, j]);
                    }
                    if (unique)
                    {
                        return false;
                    }
                }
            }
            // If everything checks out:
            return true;
        }

        // Checks if a cell can be modified
        // If so, modifies it by val and returns true
        // Else, doesn't modify the grid and returns false
        public static bool PutCell(int[,] grid, int x, int y, int val)
        {
            if (val != 0 && val != 1)
            {
                return false;
            }
            if (x >= grid.GetLength(0) || y >= grid.GetLength(1) || x < 0 || y < 0)
            {
                return false;
            }
            var oldVal = grid[x, y];
            grid[x, y] = val;
            if (!IsGridValid(grid))
            {
                grid[x, y] = oldVal;
                return false;
            }
            return true;
        }

        // Checks if a cell is set
        public static bool IsCellSet(int[,] grid, int x, int y)
        {
            return grid[x, y] != -1;
        }

        // Checks if a grid is complete
        public static bool IsGridComplete(int[,] grid)
        {
            for (var i = 0; i < grid.GetLength(0); i++)
            {
                for (var j = 0; j < grid.GetLength(1); j++)
                {
                    if (!IsCellSet(grid, i, j))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        
        // Runs an actual game of Takuzu, where the user hase to complete the grid
        // If he wins, a surprise awaits him
        public static void Game(int[,] start)
        {
            while (!IsGridComplete(start))
            {
                PrintGrid(start);
                Console.Write("x : ");
                var xInput = "";
                while (xInput == "")
                {
                    xInput = Console.ReadLine();
                }
                var x = int.Parse(xInput);
                Console.Write("y : ");
                var yInput = "";
                while (yInput == "")
                {
                    yInput = Console.ReadLine();
                }
                var y = int.Parse(yInput);
                // If cell is set, why bother to ask the user a value ?
                if (IsCellSet(start, x, y))
                {
                    Console.WriteLine("Cell is already set");
                    continue;
                }
                Console.Write("value : ");
                var valueInput = "";
                while (valueInput == "")
                {
                    valueInput = Console.ReadLine();
                }
                var value = int.Parse(valueInput);
                var valid = PutCell(start, x, y, value);
                if (!valid)
                {
                    Console.WriteLine("Your input doesn't match the rules of the game. Try again, n00b...");
                }
            }
            Console.WriteLine("You win ! Great job ;)");
        }

        // Automatically resolves a grid of Takuzu
        // At each step, prints the grid out for a nice effect
        public static int[,] AI(int[,] grid)
        {
            // Created a new class for more readability
            // Everything in the same file was messy
            return TakuzuSolver.AI(grid);
        }
    }
}