﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace Takuzu
{
    // Class used by AI2
    public class TakuzuGrid
    {
        public string[,] puzzle;
    }
    
    public static class TakuzuSolver
    {
        // Using JSON, HTTP POST and an online resource, tries to solve a grid
        // Doesn't work because parameter 'puzzle' isn't recognized for some reason
        public static void AI2(int[,] grid)
        {
            
            var rows = grid.GetLength(0);
            var columns = grid.GetLength(1);
            var stringGrid = new string[rows, columns];
            for (var i = 0; i < rows; i++)
            {
                for (var j = 0; j < columns; j++)
                {
                    if (grid[i, j] == -1)
                    {
                        stringGrid[i, j] = "-";
                    }
                    else
                    {
                        stringGrid[i, j] = grid[i, j].ToString();
                    }
                }
            }
            var takuzuGrid = new TakuzuGrid();
            takuzuGrid.puzzle = stringGrid;
            var json = JsonConvert.SerializeObject(takuzuGrid);
            Console.WriteLine(json);

            var httpWebRequest = (HttpWebRequest) WebRequest.Create("http://www.magmeister.be/binarypuzzlesolver/solve.php");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse) httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                Console.WriteLine(result);
            }
        }
        
        // AI to automatically fill the grid
        public static int[,] AI(int[,] grid)
        {
            while (!Takuzu.IsGridComplete(grid))
            {
                ReplaceTwos(grid);
                grid = Takuzu.Reverse(grid);
                ReplaceTwos(grid);
                grid = Takuzu.Reverse(grid);
                FillLine(grid);
                grid = Takuzu.Reverse(grid);
                FillLine(grid);
                grid = Takuzu.Reverse(grid);
                
            }
            return grid;
        }
        // Fills the grid using rule #2 of Takuzu
        private static void ReplaceTwos(int[,] grid)
        {
            var rows = grid.GetLength(0);
            var columns = grid.GetLength(1);
            var gridChanged = false;
            // Rows
            for (var i = 0; i < rows; i++)
            {
                for (var j = 0; j < columns - 2; j++)
                {
                    if (j == 0)
                    {
                        if (grid[i, 0] == 0 && grid[i, 1] == 0)
                        {
                            if (grid[i, 2] == 1)
                            {
                                continue;
                            }
                            Takuzu.PutCell(grid, i, j, 1);
                        }
                        else if (grid[i, 0] == 1 && grid[i, 1] == 1)
                        {
                            if (grid[i, 2] == 0)
                            {
                                continue;
                            }
                            Takuzu.PutCell(grid, i, j, 0);
                        }
                    }
                    else if (j == columns - 3)
                    {
                        if (grid[i, columns - 1] == 0 && grid[i, columns - 2] == 0)
                        {
                            if (grid[i, columns - 3] == 1)
                            {
                                continue;
                            }
                            Takuzu.PutCell(grid, i, columns - 3, 1);
                        }
                        else if (grid[i, columns - 1] == 1 && grid[i, columns - 2] == 1)
                        {
                            if (grid[i, columns - 3] == 0)
                            {
                                continue;
                            }
                            Takuzu.PutCell(grid, i, columns - 3, 0);
                        }
                    }
                    else
                    {
                        if (grid[i, j + 1] == 0 && grid[i, j + 2] == 0)
                        {
                            if (grid[i, j] != 1)
                            {
                                Takuzu.PutCell(grid, i, j, 1);
                            }
                            if (grid[i, j + 3] != 1)
                            {
                                Takuzu.PutCell(grid, i, j + 3, 1);
                            }
                        }
                        else if (grid[i, j + 1] == 1 && grid[i, j + 2] == 1)
                        {
                            if (grid[i, j] != 0)
                            {
                                Takuzu.PutCell(grid, i, j, 0);
                            }
                            if (grid[i, j + 3] != 0)
                            {
                                Takuzu.PutCell(grid, i, j + 3, 0);
                            }
                        }
                    }
                    if (grid[i, j] == 0 && grid[i, j + 2] == 0)
                    {
                        if (grid[i, j + 1] == 1)
                        {
                            continue;
                        }
                        Takuzu.PutCell(grid, i, j + 1, 1);
                    }
                    else if (grid[i, j] == 1 && grid[i, j + 2] == 1)
                    {
                        if (grid[i, j + 1] == 0)
                        {
                            continue;
                        }
                        Takuzu.PutCell(grid, i, j + 1, 0);
                    }
                }
            }
        }

        // Fills the grid using rule #1 of Takuzu
        private static void FillLine(int[,] grid)
        {
            var rows = grid.GetLength(0);
            var columns = grid.GetLength(1);
            for (var i = 0; i < rows; i++)
            {
                int zeroes, ones, others;
                Takuzu.CountRow(grid, i, out zeroes, out ones, out others);
                if (ones == columns / 2)
                {
                    for (var j = 0; j < columns; j++)
                    {
                        if (grid[i, j] == -1)
                        {
                            grid[i, j] = 0;
                        }
                    }
                }
                else if (zeroes == columns / 2)
                {
                    for (var j = 0; j < columns; j++)
                    {
                        if (grid[i, j] == -1)
                        {
                            grid[i, j] = 1;
                        }
                    }
                }
            }
        }
    }
}