﻿namespace WestWorldTycoon
{
    public class Game
    {
        private long score, money;
        private int nbRound, round;
        private Map map;
        
        public Game(string name, int nbRound, long initialMoney)
        {
            TycoonIO.GameInit(name, nbRound, initialMoney);
            round = 1;
            map = new Map(name);
            money = initialMoney;
            score = 0;
            this.nbRound = nbRound;
        }

        public long Launch(Bot bot)
        {
            bot.Start(this);
            while (round <= nbRound)
            {
                Update();
                bot.Update(this);
                round++;
            }
            bot.End(this);
            return score;
        }
        
        public void Update()
        {
            var income = map.GetIncome(map.GetPopulation());
            money += income;
            score += income;
            TycoonIO.GameUpdate();
        }

        public bool Build(int i, int j, Building.BuildingType type)
        {
            if (!map.Build(i, j, ref money, type))
            {
                return false;
            }
            TycoonIO.GameBuild(i, j, type);
            return true;
        }

        public bool Destroy(int i, int j)
        {
            if (!map.Destroy(i, j))
            {
                return false;
            }
            TycoonIO.GameDestroy(i, j);
            return true;
        }
        
        public bool Upgrade(int i, int j)
        {
            if (!map.Upgrade(i, j, ref money))
            {
                return false;
            }
            TycoonIO.GameUpgrade(i, j);
            return true;
        }
        
        public long Score
        {
            get { return score; }
        }
        
        public long Money
        {
            get { return money; }
        }
        
        public int NbRound
        {
            get { return nbRound; }
        }

        public int Round
        {
            get { return round; }
        }

        public Map Map
        {
            get { return map; }
        }
    }
}