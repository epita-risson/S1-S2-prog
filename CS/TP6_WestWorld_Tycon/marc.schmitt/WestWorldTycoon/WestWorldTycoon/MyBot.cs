﻿namespace WestWorldTycoon
{
    public class MyBot : Bot
    {
        private long _buildable, _nbrAttractions, _nbrHouses, _nbrShops;
        private long i;
        private long j;
        
        public override void Start(Game game)
        {
            
            // initializing variables
            
            // size of the map
            game.Map.GetSize(ref i, ref j);
            
            // number of places where you can build
            for (var x = 0; x < i; x++)
            {
                for (var y = 0; y < j; y++)
                {
                    if (game.Map.IsBuildable(x, y))
                    {
                        _buildable++;
                    }
                }
            }
            
            // defining the number of buildings we're going to build, based on very scientific calculations
            // much math, such wow
            _nbrShops = (long)(0.5512820512820513 * _buildable);
            while (_nbrShops % 3 != 0)
            {
                _nbrShops++;
            }
            _nbrAttractions = (long)((_buildable - _nbrShops) / 3);
            _nbrHouses = (long)((_buildable - _nbrShops) * 2 / 3);
        }

        public override void Update(Game game)
        {
            // getting how much buildings were already built
            var builtHouses = game.Map.NbrBuildings(Building.BuildingType.HOUSE);
            var builtAttractions = game.Map.NbrBuildings(Building.BuildingType.ATTRACTION);
            var builtShops = game.Map.NbrBuildings(Building.BuildingType.SHOP);

            // getting different informations to know what to do now
            var attractiveness = game.Map.GetAttractiveness();
            var housing = game.Map.GetHousing();
            
            // first round is very specific
            if (game.Round == 1)
            {
                Build(game, Building.BuildingType.SHOP, 1);
                Build(game, Building.BuildingType.HOUSE, 2);
                Build(game, Building.BuildingType.ATTRACTION, 1);
                return;
            }
            // then we build a huge amount of shops to make a lot of money
            if (builtShops < _nbrShops)
            {
               Build(game, Building.BuildingType.SHOP, _nbrShops - builtShops);
               return;
            }
            // when money exceeds a certain amount, we start to build A LOT and to upgrade A LOT
            if (game.Money > 100000)
            {
                Build(game, Building.BuildingType.ATTRACTION, _nbrAttractions - builtAttractions);
                Build(game, Building.BuildingType.HOUSE, _nbrHouses - builtHouses);

                // Sometimes upgrade from the top, so shops get upgraded too sometimes
                if (game.Round % 6 == 5)
                {
                    while(TopUpgrade(game)){} // upgrading as much as we possibly can
                }
                else
                {
                    while(BottomUpgrade(game)){} // upgrading as much as we possibly can
                }
                return;
            }
            // if money hasn't reached that amount yet, we try to keep the amount of attractiveness and housing at about the same level, in order to make as much money as possible
            if (attractiveness < housing && builtAttractions < _nbrAttractions)
            {
                Build(game, Building.BuildingType.ATTRACTION, (int)((housing - attractiveness) / Attraction.ATTRACTIVENESS[0]) + 4);
            }
            else if (housing < attractiveness && builtHouses < _nbrHouses)
            {
                Build(game, Building.BuildingType.HOUSE, (int)((attractiveness - housing) / House.HOUSING[0]) + 6);
            }
        }

        // this really isn't useful
        public override void End(Game game){}
        
        // build as much as you can
        private void Build(Game game, Building.BuildingType type)
        {
            for (var x = 0; x < i; x++)
            {
                for (var y = 0; y < j; y++)
                {
                    game.Build(x, y, type);
                }
            }
        }

        // build n buildings at the first available spot
        private void Build(Game game, Building.BuildingType type, long n)
        {
            var count = 0;
            for (var x = 0; x < i; x++)
            {
                for (var y = 0; y < j; y++)
                {
                    if (game.Build(x, y, type))
                    {
                        count++;
                    }
                    if (count == n)
                    {
                        return;
                    }
                }
            }
        }

        // destroy a building
        private bool Destroy(Game game, int x, int y)
        {
            return game.Destroy(x, y);
        }
        
        // upgrade starting at the bottom of the map
        private bool BottomUpgrade(Game game)
        {
            var upgraded = false;
            for (var x = (int) i - 1; x >= 0; x--)
            {
                for (var y = (int) j - 1; y >= 0; y--)
                {
                    if (game.Upgrade(x, y))
                    {
                        upgraded = true;
                    }
                }
            }
            return upgraded;
        }
        
        // upgrade starting at the top of the map
        private bool TopUpgrade(Game game)
        {
            var upgraded = false;
            for (var x = 0; x < i; x++)
            {
                for (var y = 0; y < j; y++)
                {
                    if (game.Upgrade(x, y))
                    {
                        upgraded = true;
                    }
                }
            }
            return upgraded;
        }
    }
}