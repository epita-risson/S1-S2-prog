﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Remoting;

namespace Genetics
{
    public class Matrix
    {
        #region Attributes

        private int Height;
        private int Width;
        public float[,] Tab;
        public float[] Bias;
        private static readonly Random Rdn = new Random();

        #endregion

        #region Constructors

        public Matrix(int height, int width, bool init = false)
        {
            Height = height;
            Width = width;
            Bias = new float[width];
            Tab = new float[height, width];
            if (init)
            {
                for (var i = 0; i < height; i++)
                {
                    for (var j = 0; j < width; j++)
                    {
                        Tab[i, j] = Rdn.Next(0, 1000) / 1000f;
                        Bias[j] = - Rdn.Next(500, 1000) / 1000f;
                    }
                }
            }
        }

        public Matrix(List<float> tab)
        {
            Height = 1;
            Width = tab.Count;
            Tab = new float[Height, Width];
            for (int j = 0; j < Width; j++)
            {
                Tab[0, j] = tab[j];
            }
        }

        #endregion

        public void MakeCopyFrom(Matrix copy)
        {
            Height = copy.Height;
            Width = copy.Width;
            Bias = new float[Width];
            Tab = new float[Height, Width];
            for (var i = 0; i < Height; i++)
            {
                for (var j = 0; j < Width; j++)
                {
                    Tab[i, j] = copy.Tab[i, j];
                    Bias[j] = copy.Bias[j];
                }
            }
        }

        public void ApplyMutation()
        {
            for (var i = 0; i < Height; i++)
            {
                for (var j = 0; j < Width; j++)
                {
                    Tab[i, j] += Rdn.Next(-1000, 1000) / 10000f;
                    Bias[j] += Rdn.Next(-1000, 1000) / 10000f;

                    if (Tab[i, j] > 1) Tab[i, j] = 1f;
                    if (Tab[i, j] < 0) Tab[i, j] = 0f;

                    /*if (Bias[j] > -.5) Bias[j] = -.5f;
                    if (Bias[j] < -1) Bias[j] = -1f;*/
                }
            }
        }

        public static Matrix operator +(Matrix a, Matrix b)
        {
            if (a.Height != b.Height || b.Width != b.Height) throw new ArgumentException("Matrices are not of the same size");

            var result = new Matrix(a.Height, a.Width);
            for (var i = 0; i < a.Height; i++)
            {
                for (var j = 0; j < a.Width; j++)
                {
                    result.Tab[i, j] = a.Tab[i, j] + b.Tab[i, j];
                    result.Bias[j] = a.Bias[j] + b.Bias[j];
                }
            }
            return result;
        }

        public static float Sigmoid(float x)
        {
            return (float) (1 / (1 + Math.Exp(-x)));
        }

        /// <summary>
        /// This is not only a multiplication ! We also normalize !
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>Normalized multiplication</returns>
        /// <exception cref="ArgumentException"></exception>
        public static Matrix operator *(Matrix a, Matrix b)
        {
            if (a.Width != b.Height) throw new ArgumentException("a's width is different from b's height");

            var result = new Matrix(a.Height, b.Width);

            for (var i = 0; i < a.Height; i++)
            {
                for (var j = 0; j < b.Width; j++)
                {
                    var s = 0f;
                    for (var k = 0; k < a.Width; k++)
                    {
                        s += a.Tab[i, k] * b.Tab[k, j];
                    }
                    result.Tab[i, j] = Sigmoid(s / b.Width + b.Bias[j]);
                }
            }
            return result;
        }


        public void Print()
        { 
            throw new NotImplementedException();
        }
    }
}