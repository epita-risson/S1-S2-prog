# -*- coding: utf-8 -*-
"""
Created on Sept. 2016
@author: nathalie
"""


class BinTree:
    def __init__(self, key, left, right):
        """
        Init Tree
        """
        self.key = key
        self.left = left
        self.right = right

    def display(self):
        print(self.key)
        if self.left is not None:
            self.left.display()
        if self.right is not None:
            self.right.display()

