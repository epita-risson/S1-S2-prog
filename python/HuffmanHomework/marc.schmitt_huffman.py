__license__ = 'GolluM & Junior (c) EPITA'
__docformat__ = 'reStructuredText'
__revision__ = '$Id: huffman.py 2018-04-24'

"""
Huffman homework
2018
@author: marc.schmitt
"""

from algopy import bintree
from algopy import heap


################################################################################
# COMPRESSION

def buildfrequencylist(dataIN):
    """
    Builds a tuple list of the character frequencies in the input.
    """
    frequencies = {}
    for c in dataIN:
        frequencies[c] = frequencies.get(c, 0) + 1
    return [(v, k) for k, v in frequencies.items()]


def __bubble_sort_priority_list(priority_list):
    """
    Sort a list containing tuples following this pattern : (number, element)
    :param priority_list:
    :return:
    """
    n = len(priority_list)
    while n != 0:
        new_n = 0
        for i in range(1, n):
            if priority_list[i - 1].key[0] < priority_list[i].key[0]:
                priority_list[i - 1], priority_list[i] = priority_list[i], priority_list[i - 1]
                new_n = i
        n = new_n
    return priority_list


def buildHuffmantree(input_list):
    """
    Processes the frequency list into a Huffman tree according to the algorithm.
    """
    priority_list = []
    for frequency in input_list:
        priority_list.append(bintree.BinTree(frequency, None, None))

    priority_list = __bubble_sort_priority_list(priority_list)

    while len(priority_list) > 1:
        el1 = priority_list.pop()
        el2 = priority_list.pop()
        els_sum = el1.key[0] + el2.key[0]
        priority_list.append(bintree.BinTree((els_sum, ""), el1, el2))
        priority_list = __bubble_sort_priority_list(priority_list)

    def tuple_tree2tree(huffmanTree):
        """
        Convert a bintree containing tuples (number, element) to a tree containing only the elements
        :param huffmanTree:
        :return:
        """
        if huffmanTree is None:
            return None
        else:
            return bintree.BinTree(huffmanTree.key[1], tuple_tree2tree(huffmanTree.left), tuple_tree2tree(huffmanTree.right))

    return tuple_tree2tree(priority_list[0])


def encodedata(huffmanTree, dataIN):
    """
    Encodes the input string to its binary string representation.
    """
    def __find_node(HT, character):
        """
        Find the binary path to an element in a bintree
        :param HT:
        :param character:
        :return:
        """
        if HT.key == character:
            return ""
        elif HT.left is None and HT.right is None:
            return "not here"
        elif HT.left is None:
            return "1" + __find_node(HT.right, character)
        elif HT.right is None:
            return "0" + __find_node(HT.left, character)
        else:
            left_binary = __find_node(HT.left, character)
            if "not here" in left_binary:
                return "1" + __find_node(HT.right, character)
            return "0" + left_binary

    result = ""
    for c in dataIN:
        result += __find_node(huffmanTree, c)
    return result


def encodetree(huffmanTree):
    """
    Encodes a huffman tree to its binary representation using a preOrder traversal:
        * each leaf key is encoded into its binary representation on 8 bits preceded by '1'
        * each time we go left we add a '0' to the result
    """
    # FIXME
    pass


def tobinary(dataIN):
    """
    Compresses a string containing binary code to its real binary value.
    """
    # FIXME
    pass


def compress(dataIn):
    """
    The main function that makes the whole compression process.
    """

    # FIXME
    pass


################################################################################
# DECOMPRESSION

def decodedata(huffmanTree, dataIN):
    """
    Decode a string using the corresponding huffman tree into something more readable.
    """
    # FIXME
    pass


def decodetree(dataIN):
    """
    Decodes a huffman tree from its binary representation:
        * a '0' means we add a new internal node and go to its left node
        * a '1' means the next 8 values are the encoded character of the current leaf
    """
    # FIXME
    pass


def frombinary(dataIN, align):
    """
    Retrieve a string containing binary code from its real binary value (inverse of :func:`toBinary`).
    """
    # FIXME
    pass


def decompress(data, dataAlign, tree, treeAlign):
    """
    The whole decompression process.
    """
    # FIXME
    pass
